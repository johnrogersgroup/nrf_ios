//
//  ApiEndpoint.swift
//  uv-guard
//
//  Created by Jacob Trueb on 2/23/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import Foundation
import UIKit
import BackgroundTasks

// MARK: Date Extensions

extension TimeZone {
    static let gmt = TimeZone(secondsFromGMT: 0)!
}
extension Formatter {
    static let date = DateFormatter()
}

extension Date {
    func localizedDescription(dateStyle: DateFormatter.Style = .medium,
                              timeStyle: DateFormatter.Style = .medium,
                              in timeZone : TimeZone = .current,
                              locale   : Locale = .current) -> String {
        Formatter.date.locale = locale
        Formatter.date.timeZone = timeZone
        Formatter.date.dateStyle = dateStyle
        Formatter.date.timeStyle = timeStyle
        return Formatter.date.string(from: self)
    }
    var localizedDescription: String { localizedDescription() }
}

// MARK: Environment Variables
public enum Environment {
  private static let infoDictionary: [String: Any] = {
    guard let dict = Bundle.main.infoDictionary else {
      fatalError("Plist file not found")
    }
    return dict
  }()

  static let apiEndpoint: String = {
    guard let endpointString = Environment.infoDictionary["API_ENDPOINT"] as? String else {
      fatalError("API_ENDPOINT URL not set in plist for this environment")
    }
    return endpointString
  }()
}

// MARK: ApiEndpoint Implementation

public class ApiEndpoint {
    
    var activeEndpoint: String!
    var devEnvironment: Bool!
    var connectable: Bool = false
    
    init() {
        self.activeEndpoint = Environment.apiEndpoint
        print("ApiEndpoint configured to communicate with \(activeEndpoint!)")
    }
    
    // MARK: Device Access
    
    static func getDevices(num_doses: Int, completion: @escaping ([Device: [RecordedDose]]) -> Void) {
        guard let token = UserDefaults.standard.value(forKey: "Token") else {
            ApiEndpoint.LOG_CLOUD("Could not get devices without token")
            return
        }
        
        let route = "device/?num_doses=\(num_doses)"
        let method = "GET"
        ApiEndpoint.makeRequest(route: route, method: method, headers: ["Authorization" : "Token \(token)"], body: nil, callback: {  (failureMessage, error, data, response) in
            guard failureMessage == nil else {
                ApiEndpoint.LOG_ERROR(failureMessage!)
                return
            }
           
            guard error == nil else {
                ApiEndpoint.LOG_ERROR("Failed with: \(error!.localizedDescription)")
                return
            }

            guard data != nil else {
                ApiEndpoint.LOG_ERROR("Failed with no data received from server")
                return
            }
           
            guard response != nil else {
                ApiEndpoint.LOG_ERROR("Failed with nil response from server")
                return
            }
           
            if response!.statusCode == 200 {
               do {
                    ApiEndpoint.LOG_TRACE(String(data: data!, encoding: String.Encoding.utf8)!)
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        guard let devices = responseJsonData["devices"] as? [[String: Any]] else {
                            ApiEndpoint.LOG_ERROR("Expected devices response for \(method) to \(route)")
                            return
                        }
                        
                        guard let doses = responseJsonData["doses"] as? [[String: Any]] else {
                            ApiEndpoint.LOG_ERROR("Expected doses response for \(method) to \(route)")
                            return
                        }
                                                
                        var userDevices: [Device: [RecordedDose]] = [:]
                        var deviceMap: [Int: Device] = [:]
                        for device in devices {
                            if let fields = device["fields"] as? [String: Any] {
                                if let id = fields["simple_device_id"] as? UInt8 {
                                    if let placement = fields["placement"] as? String {
                                        if let pk = device["pk"]  as? Int {
                                            let key = Device(id: UInt8(id), placement: placement)
                                            userDevices[key] = []
                                            deviceMap[pk] = key
                                        } else {
                                            ApiEndpoint.LOG_ERROR("device did not have a 'pk' json object for fields")
                                        }
                                    } else {
                                        ApiEndpoint.LOG_ERROR("device did not have a 'placement' json object")
                                    }
                                } else {
                                    ApiEndpoint.LOG_ERROR("device did not have a 'simple_device_id' json object")
                                }
                            } else {
                                ApiEndpoint.LOG_ERROR("device did not have a 'fields' json object")
                            }
                        }
                        
                        for dose in doses {
                            if let fields = dose["fields"] as? [String: Any] {
                                if let index = fields["index"] as? UInt8 {
                                    if let value = fields["value"] as? Float {
                                        if let recordedString = fields["recorded"] as? String {
                                            let formatter = DateFormatter()
                                            formatter.locale = Locale(identifier: "en_US_POSIX")
                                            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                                            if let recorded: Date = formatter.date(from: recordedString) {
                                                if let devicePk = fields["device"] as? Int {
                                                    userDevices[deviceMap[devicePk]!]!.append(RecordedDose(recorded: recorded, value: value, index: index))
                                                } else {
                                                    ApiEndpoint.LOG_ERROR("dose did not have a 'device' json object")
                                                }
                                            } else {
                                                ApiEndpoint.LOG_ERROR("dose recorded timestamp could not be parsed")
                                            }
                                        } else {
                                            ApiEndpoint.LOG_ERROR("dose did not have a 'recorded' json object")
                                        }
                                    } else {
                                        ApiEndpoint.LOG_ERROR("dose did not have a 'value' json object")
                                    }
                                } else {
                                    ApiEndpoint.LOG_ERROR("dose did not have a 'index' json object")
                                }
                            } else {
                                ApiEndpoint.LOG_ERROR("dose did not have a 'fields' json object")
                            }
                        }
                        
                        ApiEndpoint.LOG_DEBUG("Found devices with doses: \(userDevices)")
                        
                        
                        if userDevices.count > 4 {
                            ApiEndpoint.LOG_ERROR("devices contained more than 4 devices for a user. returning empty devices")
                            userDevices = [:]
                        }
                        
                        DispatchQueue.main.async {
                            completion(userDevices)
                        }
                   }
               } catch let error {
                   ApiEndpoint.LOG_ERROR("Getting devices failed with invalid responses: \(error.localizedDescription)")
               }
            } else {
               ApiEndpoint.LOG_ERROR("Failed to send data with http code \(response!.statusCode) and message \(data ?? Data())")
            }
        })
    }
    
    static func postDevice(device: Device, completion: @escaping (Bool) -> Void) {
        guard let token = UserDefaults.standard.value(forKey: "Token") else {
            ApiEndpoint.LOG_CLOUD("Could not get devices without token")
            return
        }
        
        let route = "device/"
        let method = "POST"
        ApiEndpoint.makeRequest(route: route,
                         method: method,
                         headers: ["Authorization" : "Token \(token)", "Content-Type": "application/json"],
                         body: ["simple_device_id": device.id, "placement": device.placement],
                         callback: {  (failureMessage, error, data, response) in
            guard failureMessage == nil else {
                ApiEndpoint.LOG_ERROR(failureMessage!)
                return
            }
           
            guard error == nil else {
                ApiEndpoint.LOG_ERROR("Failed with: \(error!.localizedDescription)")
                return
            }

            guard data != nil else {
                ApiEndpoint.LOG_ERROR("Failed with no data received from server")
                return
            }
           
            guard response != nil else {
                ApiEndpoint.LOG_ERROR("Failed with nil response from server")
                return
            }
                 
            do {
               if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    if response!.statusCode != 200 {
                       ApiEndpoint.LOG_DEBUG("Response data: \(responseJsonData)")
                    }
               }
            } catch let error {
                ApiEndpoint.LOG_ERROR("Failed to parse json response: \(error.localizedDescription)")
            }

            if response!.statusCode != 200 {
               ApiEndpoint.LOG_ERROR("Failed to send data with http code \(response!.statusCode) and message \(data ?? Data())")
            }

            completion(response!.statusCode == 200)
        })
    }
    
    static func deleteDevice(byId id: UInt8, resolve: @escaping () -> Void) {
        guard let token = UserDefaults.standard.value(forKey: "Token") else {
            ApiEndpoint.LOG_CLOUD("Could not get devices without token")
            return
        }
        
        let route = "device/"
        let method = "DELETE"
        ApiEndpoint.makeRequest(route: route,
                         method: method,
                         headers: ["Authorization" : "Token \(token)", "Content-Type": "application/json"],
                         body: ["simple_device_id": id],
                         callback: {  (failureMessage, error, data, response) in
            guard failureMessage == nil else {
                ApiEndpoint.LOG_ERROR(failureMessage!)
                return
            }
           
            guard error == nil else {
                ApiEndpoint.LOG_ERROR("Failed with: \(error!.localizedDescription)")
                return
            }

            guard data != nil else {
                ApiEndpoint.LOG_ERROR("Failed with no data received from server")
                return
            }
           
            guard response != nil else {
                ApiEndpoint.LOG_ERROR("Failed with nil response from server")
                return
            }
           
            if response!.statusCode == 200 {
               do {
                    ApiEndpoint.LOG_TRACE(String(data: data!, encoding: String.Encoding.utf8)!)
                    if let _ = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        ApiEndpoint.LOG_DEBUG("Successfully deleted \(id)")
                    }
               } catch let error {
                   ApiEndpoint.LOG_ERROR("Deleteing device failed with invalid responses: \(error.localizedDescription)")
               }
                resolve()
            } else {
               ApiEndpoint.LOG_ERROR("Failed to send data with http code \(response!.statusCode) and message \(data ?? Data())")
            }
        })
    }

    // MARK: Dose Access
    
    static func postDoses(deviceId: UInt8, toUpload dosesToUpload: [RecordedDose], completion: @escaping (_ errorMessage: String?) -> Void) {
        guard let token = UserDefaults.standard.value(forKey: "Token") else {
            ApiEndpoint.LOG_CLOUD("Could not get devices without token")
            return
        }
        
        let route = "dose/"
        let method = "POST"
        var body: [String: Any] = [:]
        var doses: [[String: String]] = []
        let formatter = ISO8601DateFormatter()
        for dose in dosesToUpload {
            doses.append([
                "device": String(deviceId),
                "index": String(dose.index!),
                "value": String(dose.value),
                "recorded": formatter.string(from: dose.recorded)
            ])
        }
        body = ["doses": doses]
    
        ApiEndpoint.makeRequest(route: route,
                         method: method,
                         headers: ["Authorization" : "Token \(token)", "Content-Type": "application/json"],
                         body: body,
                         callback: {  (failureMessage, error, data, response) in
            guard failureMessage == nil else {
                ApiEndpoint.LOG_ERROR(failureMessage!)
                completion(failureMessage)
                return
            }
           
            guard error == nil else {
                ApiEndpoint.LOG_ERROR("Failed with: \(error!.localizedDescription)")
                completion(error!.localizedDescription)
                return
            }

            guard data != nil else {
                ApiEndpoint.LOG_ERROR("Failed with no data received from server")
                completion("Failed with no data received from server")
                return
            }
           
            guard response != nil else {
                ApiEndpoint.LOG_ERROR("Failed with nil response from server")
                completion("Failed with nil response from server!")
                return
            }
 
            if response!.statusCode == 200 {
                do {
                   if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                       ApiEndpoint.LOG_TRACE("Response data: \(responseJsonData)")
                   }
                    
                    completion(nil)
                } catch let error {
                    ApiEndpoint.LOG_ERROR("Failed to parse json response: \(error.localizedDescription)")
                }
            } else {
                ApiEndpoint.LOG_ERROR("Failed to send data with http code \(response!.statusCode) and message \(data ?? Data())")
                var dataMessage: String = (data ?? Data()).description
                do {
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        ApiEndpoint.LOG_ERROR("Response data: \(responseJsonData)")
                        dataMessage = responseJsonData.description
                    }
                } catch let error {
                    ApiEndpoint.LOG_ERROR("Failed to parse json response: \(error.localizedDescription)")
                }
                completion("Failed to send data with http code \(response!.statusCode) and message \(dataMessage)")
            }
        })
    }
    
    static func getDoses(range: String?, completion: @escaping (_ errorMessage: String?, _ doses: [RecordedDose]?) -> Void) {
        guard let token = UserDefaults.standard.value(forKey: "Token") else {
            ApiEndpoint.LOG_CLOUD("Could not get devices without token")
            return
        }
        
        var days_ago = 0
        if range == "yesterday" {
            days_ago = 1
        }
        let route = range == nil ? "dose/" : "dose/?days_ago=\(days_ago)"
        let method = "GET"
        ApiEndpoint.makeRequest(route: route, method: method, headers: ["Authorization" : "Token \(token)"], body: nil, callback: {  (failureMessage, error, data, response) in
            guard failureMessage == nil else {
                ApiEndpoint.LOG_ERROR(failureMessage!)
                completion(failureMessage, nil)
                return
            }
           
            guard error == nil else {
                ApiEndpoint.LOG_ERROR("Failed with: \(error!.localizedDescription)")
                completion(error!.localizedDescription, nil)
                return
            }

            guard data != nil else {
                ApiEndpoint.LOG_ERROR("Failed with no data received from server")
                completion("Failed with no data received from server", nil)
                return
            }
           
            guard response != nil else {
                ApiEndpoint.LOG_ERROR("Failed with nil response from server")
                completion("Failed with nil response from server", nil)
                return
            }
           
            if response!.statusCode == 200 {
               do {
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        guard let doses = responseJsonData["doses"] as? [[String: Any]] else {
                            ApiEndpoint.LOG_ERROR("Expected devices response for \(method) to \(route)")
                            return
                        }
                        
                        var recordedDoses: [RecordedDose] = []
                        let formatter = DateFormatter()
                        formatter.locale = Locale(identifier: "en_US_POSIX")
                        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                        for dose in doses {
                            if let fields = dose["fields"] as? [String: Any] {
                                if let created = fields["recorded"] as? String {
                                    if let value = fields["value"] as? NSNumber {
                                        let date: Date? = formatter.date(from: created)
                                        if date != nil {
                                            recordedDoses.append(RecordedDose(recorded: date!, value: Float(truncating: value), index: nil))
                                            ApiEndpoint.LOG_TRACE("Found dose created at \(created) with value \(Float(truncating: value))")
                                        } else {
                                            ApiEndpoint.LOG_ERROR("Failed to parse date: \(created)")
                                        }
                                    } else {
                                        ApiEndpoint.LOG_ERROR("fields did not have 'value' json object")
                                    }
                                } else {
                                    ApiEndpoint.LOG_ERROR("fields did not have 'recorded' json object")
                                }
                            } else {
                                ApiEndpoint.LOG_ERROR("dose did not have 'fields' json object")
                            }
                        }
                        
                        DispatchQueue.main.async {
                            completion(nil, recordedDoses)
                        }
                   }
               } catch let error {
                    ApiEndpoint.LOG_ERROR("Getting doses failed with invalid responses: \(error.localizedDescription)")
                    completion("Getting doses failed with invalid responses: \(error.localizedDescription)", nil)
               }
            } else {
                ApiEndpoint.LOG_ERROR("Failed to send data with http code \(response!.statusCode) and message \(data ?? Data())")
                var dataMessage: String = (data ?? Data()).description
                do {
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        dataMessage = responseJsonData.description
                    }
                } catch let error {
                    dataMessage = error.localizedDescription
                }
                completion("Failed to send data with http code \(response!.statusCode) and message \(dataMessage)", nil)
            }
        })
    }

    // MARK: Weather Access
    
    static func postWeather(weatherInfo: WeatherInfo, completion: @escaping (_ errorMessage: String?) -> Void) {
        if !weatherInfo.readyToUpload() {
            ApiEndpoint.LOG_DEBUG("WeatherInfo \(weatherInfo) is not ready to upload")
            completion("WeatherInfo not ready to upload")
            return
        }
        
        guard let token = UserDefaults.standard.value(forKey: "Token") else {
            ApiEndpoint.LOG_CLOUD("Could not post weather without token")
            return
        }
        
        let route = "weather/"
        let method = "POST"
        ApiEndpoint.makeRequest(route: route, method: method, headers: ["Authorization" : "Token \(token)", "Content-Type": "application/json"], body: ["description": weatherInfo.description!, "max_uv_index": weatherInfo.max_uv_index!, "uv_index": weatherInfo.uv_index!, "temperature": weatherInfo.temperature!], callback: {  (failureMessage, error, data, response) in
            guard failureMessage == nil else {
                ApiEndpoint.LOG_ERROR(failureMessage!)
                completion(failureMessage)
                return
            }
           
            guard error == nil else {
                ApiEndpoint.LOG_ERROR("Failed with: \(error!.localizedDescription)")
                completion(error!.localizedDescription)
                return
            }

            guard data != nil else {
                ApiEndpoint.LOG_ERROR("Failed with no data received from server")
                completion("Failed with no data received from server")
                return
            }
           
            guard response != nil else {
                ApiEndpoint.LOG_ERROR("Failed with nil response from server")
                completion("Failed with nil response from server")
                return
            }
           
            if response!.statusCode == 200 {
                do {
                   if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                       ApiEndpoint.LOG_TRACE("Response data: \(responseJsonData)")
                   }
                    
                    completion(nil)
                } catch let error {
                    ApiEndpoint.LOG_ERROR("Failed to parse json response: \(error.localizedDescription)")
                }
            } else {
                ApiEndpoint.LOG_ERROR("Failed to send data with http code \(response!.statusCode) and message \(data ?? Data())")
                var dataMessage: String = (data ?? Data()).description
                do {
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        ApiEndpoint.LOG_ERROR("Response data: \(responseJsonData)")
                        dataMessage = responseJsonData.description
                    }
                } catch let error {
                    ApiEndpoint.LOG_ERROR("Failed to parse json response: \(error.localizedDescription)")
                }
                completion("Failed to send data with http code \(response!.statusCode) and message \(dataMessage)")
            }
        })
    }
    
    // MARK: Notification Access
    
    static func postNotification(deviceToken: String) {
        guard let token = UserDefaults.standard.value(forKey: "Token") else {
            ApiEndpoint.LOG_CLOUD("Could not post notification registration without token")
            return
        }
        
        let route = "notification/"
        let method = "POST"
        ApiEndpoint.makeRequest(route: route, method: method, headers: ["Authorization" : "Token \(token)", "Content-Type": "application/json"], body: ["registration_id": deviceToken], callback: {  (failureMessage, error, data, response) in
            guard failureMessage == nil else {
                ApiEndpoint.LOG_ERROR(failureMessage!)
                return
            }
           
            guard error == nil else {
                ApiEndpoint.LOG_ERROR("Failed with: \(error!.localizedDescription)")
                return
            }

            guard data != nil else {
                ApiEndpoint.LOG_ERROR("Failed with no data received from server")
                return
            }
           
            guard response != nil else {
                ApiEndpoint.LOG_ERROR("Failed with nil response from server")
                return
            }
           
            if response!.statusCode == 200 {
               do {
                    ApiEndpoint.LOG_TRACE(String(data: data!, encoding: String.Encoding.utf8)!)
                    if let _ = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        return;
                    }
               } catch let error {
                   ApiEndpoint.LOG_ERROR("Posting weather failed with invalid responses: \(error.localizedDescription)")
               }
            } else {
               ApiEndpoint.LOG_ERROR("Failed to send data with http code \(response!.statusCode) and message \(data ?? Data())")
            }
        })
    }

    // MARK: Core Request
    
    static let TASK_ID_PREFIX = "com.wearifi.uv-guard.make-request"
    static var ACTIVE_TASKS: [String: UIBackgroundTaskIdentifier] = [:]


    static func makeRequest(route: String, method: String, headers: [String: String], body: [String: Any]?, callback: @escaping (String?, Error?, Data?, HTTPURLResponse?) -> Void) {
        // Configure a request for the url
        let taskIdentifier = "\(TASK_ID_PREFIX).\(method.lowercased()).\(route.replacingOccurrences(of: "/", with: ""))"
        var backgroundIdentifier = taskIdentifier
        DispatchQueue.main.async {
            for i in 0..<256 {
                backgroundIdentifier = "\(taskIdentifier).\(i)"
                if ACTIVE_TASKS[backgroundIdentifier] != nil && ACTIVE_TASKS[backgroundIdentifier] != UIBackgroundTaskIdentifier.invalid {
                    continue
                } else {
                    break
                }
            }
            
            // Prepare the URL session
            let url = URL(string: (UIApplication.shared.delegate as! AppDelegate).apiEndpoint.activeEndpoint + route)
            var request = URLRequest(url: url!)
            request.httpMethod = method
            for (key, value) in headers {
                request.addValue(value, forHTTPHeaderField: key)
            }
            if let body = body {
                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
                } catch {
                    ApiEndpoint.LOG_DEBUG("Finished background task with identifier: \(backgroundIdentifier) (\(ACTIVE_TASKS[backgroundIdentifier]!))")
                    UIApplication.shared.endBackgroundTask(ACTIVE_TASKS[backgroundIdentifier]!)
                    ACTIVE_TASKS[backgroundIdentifier] = UIBackgroundTaskIdentifier.invalid
                    DispatchQueue.main.async {
                        callback("Failed with: \(error.localizedDescription)", nil, nil, nil)
                    }
                    return
                }
            }
            
            // Perform the request off the main thread
            let sessionTask = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let httpResponse = response as! HTTPURLResponse? else {
                    ApiEndpoint.LOG_ERROR("Failed to downcast response to HTTPURLResponse after http request")
                    ApiEndpoint.LOG_DEBUG("Finished background task with identifier: \(backgroundIdentifier) (\(ACTIVE_TASKS[backgroundIdentifier]!))")
                    UIApplication.shared.endBackgroundTask(ACTIVE_TASKS[backgroundIdentifier]!)
                    ACTIVE_TASKS[backgroundIdentifier] = UIBackgroundTaskIdentifier.invalid
                    DispatchQueue.main.async {
                        callback("Failed to downcast response to HTTPURLResponse after http request: \(String(describing: response))", nil, nil, nil)
                    }
                    return
                }
                
                ApiEndpoint.LOG_DEBUG("Finished background task with identifier: \(backgroundIdentifier) (\(ACTIVE_TASKS[backgroundIdentifier]!))")
                UIApplication.shared.endBackgroundTask(ACTIVE_TASKS[backgroundIdentifier]!)
                ACTIVE_TASKS[backgroundIdentifier] = UIBackgroundTaskIdentifier.invalid
                DispatchQueue.main.async {
                    callback(nil, error, data, httpResponse)
                }
            }
            sessionTask.priority = 1
            
            // Register the short background task
            ACTIVE_TASKS[backgroundIdentifier] = UIApplication.shared.beginBackgroundTask(withName: backgroundIdentifier) {
                ApiEndpoint.LOG_DEBUG("Finished background task with identifier due to expiration: \(backgroundIdentifier) (\(ACTIVE_TASKS[backgroundIdentifier]!))")
                sessionTask.cancel()
                if ACTIVE_TASKS[backgroundIdentifier] != UIBackgroundTaskIdentifier.invalid {
                    UIApplication.shared.endBackgroundTask(ACTIVE_TASKS[backgroundIdentifier]!)
                    ACTIVE_TASKS[backgroundIdentifier] = UIBackgroundTaskIdentifier.invalid
                }
            }
            
            // Start the URL session
            ApiEndpoint.LOG_DEBUG("Beginning background task with identifier: \(backgroundIdentifier) (\(ACTIVE_TASKS[backgroundIdentifier]!))")
            sessionTask.resume()
        }
    }
    
    // MARK: LOGGER
    
    // Log messages that are sent to the endpoint when possible
    static let CLOUD = "[CLOUD]" // 0
    static let ERROR = "[ERROR]" // 1

    static let DEBUG = "[DEBUG]" // 2
    static let TRACE = "[TRACE]" // 3 : Development only
    
    public static var TEST = false
    
    func _LOG(formattedMessage: String) {
        if ApiEndpoint.TEST {
            print(formattedMessage)
        } else {
            NSLog(formattedMessage)
        }
    }
    
    static func LOG(formattedMessage: String, doUpload: Bool) {
//        if connectable && doUpload {
//            // TODO: Send CLOUD and ERROR logs to the endpoint
//        }
        DispatchQueue.main.async {
            (UIApplication.shared.delegate as! AppDelegate).apiEndpoint._LOG(formattedMessage: formattedMessage)
        }
    }
    
    static func LOG_CLOUD(_ message: String) {
        LOG(formattedMessage: "\(CLOUD) :: \(message)", doUpload: true)
    }

    static func LOG_ERROR(_ message: String) {
        LOG(formattedMessage: "\(ERROR) :: \(message)", doUpload: true)
    }

    static func LOG_DEBUG(_ message: String) {
        LOG(formattedMessage: "\(DEBUG) :: \(message)", doUpload: false)
    }

    static func LOG_TRACE(_ message: String) {
//        LOG(formattedMessage: "\(TRACE) :: \(message)", doUpload: false)
    }
    
}
