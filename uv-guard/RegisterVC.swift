//
//  RegisterVC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 3/5/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {
    
    
    @IBOutlet weak var registerStack: UIStackView!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiEndpoint.LOG_DEBUG("Loaded RegisterVC")
        
        self.navigationController?.view.hideToastActivity()
        usernameTextField.text = UserDefaults.standard.value(forKey: "Username") as? String
        usernameTextField.delegate = self
        passwordTextField.text = UserDefaults.standard.value(forKey: "Password") as? String
        passwordTextField.delegate = self

        self.tapOutOfKeyboard()
    }
        
    @IBAction func doRegister(_ sender: Any) {
        let username = usernameTextField.text!
        ApiEndpoint.LOG_DEBUG("doRegister with '\(username)'")

        // Validate Username
        let usernameRegex = "[A-Z0-9a-z]{4,32}"
        let usernamePredicate = NSPredicate(format:"SELF MATCHES %@", usernameRegex)
        let usernameIsValid = usernamePredicate.evaluate(with: username)
        if(usernameIsValid) {
            ApiEndpoint.LOG_DEBUG("\(username) is considered valid. Proceding with authentication ...")
            UserDefaults.standard.setValue(username, forKey: "Username")
        } else {
            self.navigationController?.view.makeToast("\(username) is considered invalid.", position: .center)
            return
        }
        
        self.navigationController?.view.makeToastActivity(.center)
        
        sendRegistrationRequest(username: username, password: passwordTextField.text!)
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        registerStack.bindToKeyboard()
//    }
//    
//    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        registerStack.unbindToKeyboard()
//    }

    func sendRegistrationRequest(username: String, password: String) {
        ApiEndpoint.makeRequest(route: "register-user/", method: "POST", headers: ["Content-Type": "application/json"], body: ["username": username, "password": password], callback: { (failureMessage, error, data, httpResponse) in
            guard failureMessage == nil else {
                self.failWithMessage(message: failureMessage!)
                return
            }
            
            guard error == nil else {
                self.failWithMessage(message: "Failed to register with: \(error!.localizedDescription)")
                return
            }

            guard let data = data else {
                self.failWithMessage(message: "Failed to register with empty response from server")
                return
            }
            
            guard let httpResponse = httpResponse else {
                self.failWithMessage(message: "Failed to register with invalid response from server")
                return
            }
            
            if httpResponse.statusCode == 200 {
                UserDefaults.standard.set(username, forKey: "Username")
                UserDefaults.standard.set(password, forKey: "Password")
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: false)
                }
            } else {
                self.failWithMessage(message: "Registration failed with code \(httpResponse.statusCode). \(self.getJsonMessage(data))")
            }
        })
    }
    
    func printJsonPayload(_ jsonData: Data) {
        do {
            if let responseJsonData = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any] {
                ApiEndpoint.LOG_TRACE("Received response with payload \(responseJsonData)")
            }
        } catch let error {
            ApiEndpoint.LOG_TRACE("Encountered error parsing json response data: \(error.localizedDescription)")
        }

    }
    
    func getJsonMessage(_ jsonData: Data) -> String {
        do {
            if let responseJsonData = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any] {
                ApiEndpoint.LOG_TRACE("Received response with payload \(responseJsonData)")
                if let message = responseJsonData["message"] as? String {
                    return message
                }
            }
        } catch let error {
            ApiEndpoint.LOG_DEBUG("Encountered error parsing json response data: \(error.localizedDescription)")
        }
        return ""
    }
    
    func failWithMessage(message: String) {
        ApiEndpoint.LOG_ERROR(message)
        DispatchQueue.main.async {
            self.navigationController?.view.hideToastActivity()
            self.navigationController?.view.makeToast("There was a problem registering: \(message)", position: .center)
        }
    }
}

extension RegisterVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

