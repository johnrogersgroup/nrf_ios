//
//  SettingsVC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 3/6/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
            
    @IBAction func doLogout() {
        UserDefaults.standard.set(nil, forKey: "Username")
        UserDefaults.standard.set(nil, forKey: "Password")
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func resetSurvey(_ sender: Any) {
        UserDefaults.standard.set(nil, forKey: "SurveyResult")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        guard UserDefaults.standard.string(forKey: "Username") != nil else {
            return
        }
        
        guard UserDefaults.standard.string(forKey: "Password") != nil else {
            return
        }
        
    }
}
