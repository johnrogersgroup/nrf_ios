//
//  AgeVC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/26/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import Foundation
import UIKit

class AgeVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
        
    @IBOutlet weak var mPicker: UIPickerView!
    
    var mPickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        for i in 18...128 {
            mPickerData.append(String(describing: i))
        }
        mPicker.delegate = self
        mPicker.dataSource = self
        mPicker.selectRow(0, inComponent: 0, animated: true)
        UserDefaults.standard.set(mPickerData[mPicker.selectedRow(inComponent: 0)], forKey: "Age")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UserDefaults.standard.set(mPickerData[mPicker.selectedRow(inComponent: 0)], forKey: "Age")
        super.viewDidDisappear(animated)
    }

       override func didReceiveMemoryWarning() {
           super.didReceiveMemoryWarning()
           // Dispose of any resources that can be recreated.
       }
    
       // Number of columns of data
       func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }
       
       // The number of rows of data
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
           return mPickerData.count
       }
       
       // The data to return fopr the row and component (column) that's being passed in
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           return mPickerData[row]
       }
}
