//
//  SkinCancer1VC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/26/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import Foundation
import UIKit

class SkinCancer1VC: UIViewController {
    
    
    @IBOutlet weak var noneSwitch: UISwitch!
    @IBOutlet weak var grandparentSwitch: UISwitch!
    @IBOutlet weak var parentSwitch: UISwitch!
    @IBOutlet weak var siblingSwitch: UISwitch!
    @IBOutlet weak var childSwitch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noneSwitch.isOn = true
        grandparentSwitch.isOn = false
        parentSwitch.isOn = false
        siblingSwitch.isOn = false
        childSwitch.isOn = false
    }
    
    @IBAction func selectedNone(_ sender: Any) {
        noneSwitch.isOn = true
        grandparentSwitch.isOn = false
        parentSwitch.isOn = false
        siblingSwitch.isOn = false
        childSwitch.isOn = false
    }
    
    @IBAction func selectedNotNone(_ sender: Any) {
        noneSwitch.isOn = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        var selectedSwitches: String = ""
        if noneSwitch.isOn {
            selectedSwitches = "none"
        } else {
            var firstValueSet = false
            if grandparentSwitch.isOn {
                selectedSwitches = "grandparent"
                firstValueSet = true
            }
            
            if parentSwitch.isOn {
                if firstValueSet {
                    selectedSwitches += ","
                } else {
                    firstValueSet = true
                }
                selectedSwitches += "parent"
            }
            
            if siblingSwitch.isOn {
                if firstValueSet {
                    selectedSwitches += ","
                } else {
                    firstValueSet = true
                }
                selectedSwitches += "sibling"
            }
            
            if childSwitch.isOn {
                if firstValueSet {
                    selectedSwitches += ","
                } else {
                    firstValueSet = true
                }
                selectedSwitches += "child"
            }
        }
        UserDefaults.standard.set(selectedSwitches, forKey: "SkinCancer1")
        super.viewDidDisappear(animated)
    }
}
