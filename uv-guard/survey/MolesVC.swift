//
//  MolesVC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/26/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import Foundation
import UIKit

class MolesVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
        
    @IBOutlet weak var mPicker: UIPickerView!
    
    var mPickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mPickerData = ["None", "1-3", "4-10", "More than 10"]
        mPicker.delegate = self
        mPicker.dataSource = self
    }
    
       override func didReceiveMemoryWarning() {
           super.didReceiveMemoryWarning()
           // Dispose of any resources that can be recreated.
       }
    
       // Number of columns of data
       func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }
       
       // The number of rows of data
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
           return mPickerData.count
       }
       
       // The data to return fopr the row and component (column) that's being passed in
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           return mPickerData[row]
       }
}
