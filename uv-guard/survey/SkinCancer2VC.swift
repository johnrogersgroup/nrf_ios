//
//  SkinCancer2VC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/26/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import Foundation
import UIKit

class SkinCancer2VC: UIViewController {
    
    @IBOutlet weak var noneSwitch: UISwitch!
    @IBOutlet weak var basicCellSwitch: UISwitch!
    @IBOutlet weak var squamousCellSwitch: UISwitch!
    @IBOutlet weak var melanomaSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noneSwitch.isOn = true
        basicCellSwitch.isOn = false
        squamousCellSwitch.isOn = false
        melanomaSwitch.isOn = false
    }
    
    @IBAction func selectedNone(_ sender: Any) {
        noneSwitch.isOn = true
        basicCellSwitch.isOn = false
        squamousCellSwitch.isOn = false
        melanomaSwitch.isOn = false
    }
    
    @IBAction func selectedNotNone(_ sender: Any) {
        noneSwitch.isOn = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        var selectedSwitches: String = ""
        if noneSwitch.isOn {
            selectedSwitches = "none"
        } else {
            var firstValueSet = false
            if basicCellSwitch.isOn {
                selectedSwitches = "basicCell"
                firstValueSet = true
            }
            
            if squamousCellSwitch.isOn {
                if firstValueSet {
                    selectedSwitches += ","
                } else {
                    firstValueSet = true
                }
                selectedSwitches += "squamousCell"
            }
            
            if melanomaSwitch.isOn {
                if firstValueSet {
                    selectedSwitches += ","
                } else {
                    firstValueSet = true
                }
                selectedSwitches += "melanoma"
            }
        }
        UserDefaults.standard.set(selectedSwitches, forKey: "SkinCancer2")
        super.viewDidDisappear(animated)
    }
}
