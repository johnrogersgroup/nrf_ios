//
//  HispanicVC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/26/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import Foundation
import UIKit

class SkinColorVC: UIViewController {
            
    @IBOutlet weak var skin1: UIButton!
    @IBOutlet weak var skin2: UIButton!
    @IBOutlet weak var skin3: UIButton!
    @IBOutlet weak var skin4: UIButton!
    @IBOutlet weak var skin5: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        skin1.layer.borderWidth = 2
        skin2.layer.borderWidth = 2
        skin3.layer.borderWidth = 2
        skin4.layer.borderWidth = 2
        skin5.layer.borderWidth = 2
        
        skin1.layer.cornerRadius = 5
        skin2.layer.cornerRadius = 5
        skin3.layer.cornerRadius = 5
        skin4.layer.cornerRadius = 5
        skin5.layer.cornerRadius = 5

        skin1.layer.borderColor = UIColor.white.cgColor
        skin2.layer.borderColor = UIColor.white.cgColor
        skin3.layer.borderColor = UIColor.white.cgColor
        skin4.layer.borderColor = UIColor.white.cgColor
        skin5.layer.borderColor = UIColor.white.cgColor
        
        continueButton.isEnabled = false
    }
    
    @IBAction func selectSkin1(_ sender: Any) {
        UserDefaults.standard.set("bright white", forKey: "SkinColor")
        skin1.layer.borderColor = UIColor.systemBlue.cgColor
        skin2.layer.borderColor = UIColor.white.cgColor
        skin3.layer.borderColor = UIColor.white.cgColor
        skin4.layer.borderColor = UIColor.white.cgColor
        skin5.layer.borderColor = UIColor.white.cgColor
        
        continueButton.isEnabled = true
    }
    
    @IBAction func selectSkin2(_ sender: Any) {
        UserDefaults.standard.set("white", forKey: "SkinColor")
        skin1.layer.borderColor = UIColor.white.cgColor
        skin2.layer.borderColor = UIColor.systemBlue.cgColor
        skin3.layer.borderColor = UIColor.white.cgColor
        skin4.layer.borderColor = UIColor.white.cgColor
        skin5.layer.borderColor = UIColor.white.cgColor
        
        continueButton.isEnabled = true
    }
    
    @IBAction func selectSkin3(_ sender: Any) {
        UserDefaults.standard.set("fair", forKey: "SkinColor")
        skin1.layer.borderColor = UIColor.white.cgColor
        skin2.layer.borderColor = UIColor.white.cgColor
        skin3.layer.borderColor = UIColor.systemBlue.cgColor
        skin4.layer.borderColor = UIColor.white.cgColor
        skin5.layer.borderColor = UIColor.white.cgColor
        
        continueButton.isEnabled = true
    }
    
    @IBAction func selectSkin4(_ sender: Any) {
        UserDefaults.standard.set("light", forKey: "SkinColor")
        skin1.layer.borderColor = UIColor.white.cgColor
        skin2.layer.borderColor = UIColor.white.cgColor
        skin3.layer.borderColor = UIColor.white.cgColor
        skin4.layer.borderColor = UIColor.systemBlue.cgColor
        skin5.layer.borderColor = UIColor.white.cgColor
        
        continueButton.isEnabled = true
    }
    
    @IBAction func selectSkin5(_ sender: Any) {
        UserDefaults.standard.set("brown", forKey: "SkinColor")
        skin1.layer.borderColor = UIColor.white.cgColor
        skin2.layer.borderColor = UIColor.white.cgColor
        skin3.layer.borderColor = UIColor.white.cgColor
        skin4.layer.borderColor = UIColor.white.cgColor
        skin5.layer.borderColor = UIColor.systemBlue.cgColor
        
        continueButton.isEnabled = true
    }
}
