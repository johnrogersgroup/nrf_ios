//
//  DoseService.swift
//  uv-guard
//
//  Created by Jacob Trueb on 6/1/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import UIKit
import Foundation
import BackgroundTasks


public class DoseService {
    
    // MARK: Internal Structures
            
    // Order all of the doses by index for each device
    struct Interval {
        var begin: UInt8
        var end: UInt8
        var id: Int
        var doses: [RecordedDose: Int] = [:]
        
        func contains(_ dose: RecordedDose) -> Bool {
            if begin < end {
                // |- - - -BEGIN---CONTAINED---END- - - -|
                return dose.index! >= begin && dose.index! <= end
            } else {
                // |---CONTAINED---END- - - - -BEGIN----CONTAINED----|
                return (dose.index! >= begin && dose.index! <= 255) || (dose.index! >= 0 && dose.index! <= end)
            }
        }
        
        func adjacent(_ dose: RecordedDose) -> Bool {
            if begin == 0 && dose.index! == 255 {
                return true
            } else if end == 255 && dose.index! == 0 {
                return true
            } else if begin &- 1 == dose.index! {
                return true
            } else if end &+ 1 == dose.index! {
                return true
            }
            
            return false
        }
        
        func adjacent(_ interval: Interval) -> Bool {
            if begin == 0 && interval.end == 255 {
                return true
            } else if end == 255 && interval.begin == 0 {
                return true
            } else if begin &- 1 == interval.end {
                return true
            } else if end &+ 1 == interval.begin {
                return true
            }
            
            return false
        }
        
        mutating func add(_ dose: RecordedDose) {
            if doses[dose] == nil {
                doses[dose] = 1
            } else {
                doses[dose]! += 1
            }
            
            if (begin &- 1) == dose.index! {
                begin = dose.index!
            } else if (end &+ 1) == dose.index! {
                end = dose.index!
            }
        }
        
        mutating func add(_ interval: Interval) {
            for (dose, count) in interval.doses {
                if doses[dose] == nil {
                    doses[dose] = count
                } else {
                    doses[dose]! += count
                }
            }
            
            if (end &+ 1) == interval.begin {
                end = interval.end
            } else if (begin &- 1) == interval.end {
                begin = interval.begin
            } else {
                fatalError()
            }
        }
    }
    
    // MARK: Background Scheduler
    
    static let TASK_ID = "com.wearifi.uv-guard.make-request"
    static var REGISTERED = false
    static var REGISTER_LOCK = pthread_mutex_t()
    static var REQUEST_LOCK = pthread_mutex_t()
    static var PENDING_PAYLOAD_LOCK = pthread_mutex_t()
    @Atomic static var OUTSTANDING_REQUESTS = 0
    @Atomic static var ACTIVE_BG_SHORT_TASK_ID = UIBackgroundTaskIdentifier.invalid
    
    static func registerBackgroundIdentifiers() {
         pthread_mutex_lock(&REGISTER_LOCK)
         if REGISTERED {
             pthread_mutex_unlock(&REGISTER_LOCK)
             return
         }
        
        // Register the identifier that we will use for app refresh
        for taskIdentifier in [TASK_ID] {
             ApiEndpoint.LOG_DEBUG("Registering BGTaskScheduler launch handler for \(taskIdentifier)")
             BGTaskScheduler.shared.register(forTaskWithIdentifier: taskIdentifier, using: nil) { (task) in
                ApiEndpoint.LOG_DEBUG("Launch handler called for \(task)")
                DoseService.handleMakeRequest(task: task)
             }
         }
         
         REGISTERED = true
         pthread_mutex_unlock(&REGISTER_LOCK)
     }
    
    static func handleMakeRequest(task: BGTask?) {
        // Prevent any other thread from trying to make a request
        ApiEndpoint.LOG_DEBUG("Waiting for REQUEST_LOCK ...")
        pthread_mutex_lock(&REQUEST_LOCK)
        ApiEndpoint.LOG_DEBUG("REQUEST_LOCK acquired.")
        
        ApiEndpoint.LOG_DEBUG("Waiting for PENDING_PAYLOAD_LOCK ...")
        pthread_mutex_lock(&PENDING_PAYLOAD_LOCK)
        ApiEndpoint.LOG_DEBUG("PENDING_PAYLOAD_LOCK acquired.")
        
        // Freeze the doses that will be uploaded
        stagedPayloads = stagedPayloads + pendingPayloads
        pendingPayloads = []

        pthread_mutex_unlock(&PENDING_PAYLOAD_LOCK)
        ApiEndpoint.LOG_DEBUG("PENDING_PAYLOAD_LOCK released.")

        
        if ACTIVE_BG_SHORT_TASK_ID != UIBackgroundTaskIdentifier.invalid {
            fatalError("There may never be 2 ongoing background dose requests")
        }
        
        if task == nil {
            ACTIVE_BG_SHORT_TASK_ID = UIApplication.shared.beginBackgroundTask(withName: TASK_ID + ".0")
            ApiEndpoint.LOG_DEBUG("Beginning background task with identifier: \(TASK_ID + ".0") (\(ACTIVE_BG_SHORT_TASK_ID))")
        }
                
        // Get weather info around the same time that the dose is recorded
        OUTSTANDING_REQUESTS = 1
        let weatherInfo = WeatherInfo()
        
        // Reduce advertisements for upload (duplicates and overlaps)
        let finalPayloads = consolidateDoses(uploadingPayloads: stagedPayloads)
        stagedPayloads = finalPayloads
        stagedPayloadsSuccessfullyUploaded = true
        OUTSTANDING_REQUESTS += finalPayloads.count

        // Upload all of the weather
        Weather.populateUvIndex(weatherInfo: weatherInfo, completion: { (weatherInfo) in
            ApiEndpoint.LOG_DEBUG("Populated UV Index for dose")
            Weather.populateMaxUvIndex(weatherInfo: weatherInfo, completion: { (weatherInfo) in
                ApiEndpoint.LOG_DEBUG("Populated Max UV Index for dose")
                Weather.populateWeather(weatherInfo: weatherInfo, completion: { (weatherInfo) in
                    ApiEndpoint.LOG_DEBUG("Populated Weather for dose")
                    ApiEndpoint.postWeather(weatherInfo: weatherInfo, completion: { (errorMessage) in
                        if let message = errorMessage {
                            ApiEndpoint.LOG_ERROR("Failed to postWeather: \(message)")
                        }
                        
                        OUTSTANDING_REQUESTS -= 1
                        if OUTSTANDING_REQUESTS == 0 {
                            if stagedPayloadsSuccessfullyUploaded {
                                ApiEndpoint.LOG_DEBUG("Last OUTSTANDING_REQUEST finished successfully for all dose uploads.")
                                stagedPayloads = []
                            }

                            if task == nil {
                                ApiEndpoint.LOG_DEBUG("Uploaded a payload from a BackgroundTask")
                                ApiEndpoint.LOG_DEBUG("Finished background task with identifier: \(TASK_ID + ".0") (\(ACTIVE_BG_SHORT_TASK_ID))")
                                UIApplication.shared.endBackgroundTask(ACTIVE_BG_SHORT_TASK_ID)
                                ACTIVE_BG_SHORT_TASK_ID = UIBackgroundTaskIdentifier.invalid
                            } else {
                                ApiEndpoint.LOG_DEBUG("Uploaded a payload from a BGAppRefreshTask")
                                task!.setTaskCompleted(success: true)
                            }
                            
                            // Allow the next task to be scheduled
                            DoseService.isScheduledForRefresh = false
                            ApiEndpoint.LOG_DEBUG("Finished request with uploading weather")
                            
                            
                            // Allow another thread to attempt request
                            pthread_mutex_unlock(&REQUEST_LOCK)
                            ApiEndpoint.LOG_DEBUG("Released REQUEST_LOCK.")
                            
                            // Continue making requests
                            ApiEndpoint.LOG_DEBUG("DoseService finished makeRequest() with \(pendingPayloads.count) pending payloads and \(stagedPayloads.count) staged payloads")
                            if pendingPayloads.count > 0 || stagedPayloads.count > 0 {
                                addPayload(advertisement: nil)
                            }
                        }
                    })
                })
            })
        })
        

        // Upload all of the pending doses
        ApiEndpoint.LOG_DEBUG("Posting doses in consolidated payloads: \(finalPayloads)")
        for payload in finalPayloads {
            ApiEndpoint.postDoses(deviceId: payload.getDeviceId(), toUpload: payload.getDoses(), completion: { (errorMessage) in
                if let message = errorMessage {
                    ApiEndpoint.LOG_ERROR("Failed to postDoses: \(message)")
                    stagedPayloadsSuccessfullyUploaded = false
                }
                
                OUTSTANDING_REQUESTS -= 1
                if OUTSTANDING_REQUESTS == 0 {
                    if stagedPayloadsSuccessfullyUploaded {
                        ApiEndpoint.LOG_DEBUG("Last OUTSTANDING_REQUEST finished successfully for all dose uploads.")
                        stagedPayloads = []
                    }
                    
                    if task == nil {
                        ApiEndpoint.LOG_DEBUG("Uploaded a payload from a BackgroundTask. Finished uploading doses")
                        ApiEndpoint.LOG_DEBUG("Finished background task with identifier: \(TASK_ID + ".0") (\(ACTIVE_BG_SHORT_TASK_ID))")
                        UIApplication.shared.endBackgroundTask(ACTIVE_BG_SHORT_TASK_ID)
                        ACTIVE_BG_SHORT_TASK_ID = UIBackgroundTaskIdentifier.invalid
                    } else {
                        ApiEndpoint.LOG_DEBUG("Uploaded a payload from a BGAppRefreshTask. Finished uploading doses")
                        task!.setTaskCompleted(success: true)
                    }
                    
                    // Allow the next task to be scheduled
                    DoseService.isScheduledForRefresh = false
                    ApiEndpoint.LOG_DEBUG("Finished request with uploading doses")
                    
                    // Allow another thread to attempt request
                    pthread_mutex_unlock(&REQUEST_LOCK)
                    ApiEndpoint.LOG_DEBUG("Released REQUEST_LOCK.")
                   
                    // Continue making requests
                    ApiEndpoint.LOG_DEBUG("DoseService finished makeRequest() with \(pendingPayloads.count) pending payloads and \(stagedPayloads.count) staged payloads")
                    if pendingPayloads.count > 0 || stagedPayloads.count > 0 {
                        addPayload(advertisement: nil)
                    }
                }
            })
        }
    }
    
    public static func consolidateDoses(uploadingPayloads: [AdvertisementPayload]) -> [AdvertisementPayload] {
        // Collect all of the doses by device
        var uniqueDosesByDevId: [UInt8: [RecordedDose]] = [:]
        for payload in uploadingPayloads {
            if uniqueDosesByDevId[payload.getDeviceId()] == nil {
                uniqueDosesByDevId[payload.getDeviceId()] = payload.getDoses()
            } else {
                uniqueDosesByDevId[payload.getDeviceId()]! += payload.getDoses()
            }
        }
        
        var intervals: [UInt8: [Interval]] = [:]
        var intervalId = 0
        for (dId, dDoses) in uniqueDosesByDevId {
            if intervals[dId] == nil {
                intervals[dId] = []
            }
            
            var deviceIntervals: [Interval] = intervals[dId]!
            for dose in dDoses {
                // base case
                if deviceIntervals.count == 0 {
                    ApiEndpoint.LOG_DEBUG("First Interval \(intervalId) for \(dose)")
                    var interval = Interval(begin: dose.index!, end: dose.index!, id: intervalId)
                    interval.add(dose)
                    deviceIntervals.append(interval)
                    intervalId += 1
                    continue
                }
                
                // exists in interval
                var foundInterval = false
                for idx in deviceIntervals.indices {
                    if deviceIntervals[idx].contains(dose) {
                        ApiEndpoint.LOG_DEBUG("Existing Interval \(deviceIntervals[idx].id) for \(dose)")
                        deviceIntervals[idx].add(dose)
                        foundInterval = true
                        break
                    }
                }
                
                // COPIED BELOW
                if foundInterval {
                    deviceIntervals = combineAdjacent(deviceIntervals: &deviceIntervals)
                    continue
                }

                // extends interval
                for idx in deviceIntervals.indices {
                    if deviceIntervals[idx].adjacent(dose) {
                        ApiEndpoint.LOG_DEBUG("Extends Interval \(deviceIntervals[idx].id) for \(dose)")
                        deviceIntervals[idx].add(dose)
                        foundInterval = true
                        break
                    }
                }
                
                // COPIED ABOVE
                if foundInterval {
                    deviceIntervals = combineAdjacent(deviceIntervals: &deviceIntervals)
                    continue
                }
                
                // starts new interval
                ApiEndpoint.LOG_DEBUG("New Interval \(intervalId) for \(dose)")
                var interval = Interval(begin: dose.index!, end: dose.index!, id: intervalId)
                interval.add(dose)
                deviceIntervals.append(interval)
                intervalId += 1
                deviceIntervals = combineAdjacent(deviceIntervals: &deviceIntervals)
            }
            
            intervals[dId] = deviceIntervals
        }
        
        // Create an advertisement payload for each continguous range of doses
        var postablePayloads: [AdvertisementPayload] = []
        for (dId, dIntervals) in intervals {
            for interval in dIntervals {
                ApiEndpoint.LOG_DEBUG("Interval \(interval.id) for Device \(dId):")
                var dosesByIndex: [UInt8: RecordedDose] = [:]
                var doseCounts: [UInt8: Int] = [:]
                for (dose, count) in interval.doses {
                    if doseCounts[dose.index!] == nil {
                        doseCounts[dose.index!] = count
                        dosesByIndex[dose.index!] = dose
                    } else if doseCounts[dose.index!]! < count {
                        doseCounts[dose.index!] = count
                        dosesByIndex[dose.index!] = dose
                    }
                }
                
                var dosesIndexes: [UInt8] = []
                for (index, _) in dosesByIndex {
                    dosesIndexes.append(index)
                }
                dosesIndexes.sort(by: { $0 > $1 })
                
                var intervalDoses: [RecordedDose] = []
                for index in dosesIndexes {
                    intervalDoses.append(dosesByIndex[index]!)
                }
    
                // Reorder the UInt8 overflow so that the beginning comes first
                if interval.begin > interval.end {
                    intervalDoses += intervalDoses[0..<Int(interval.end)]
                    intervalDoses = Array(intervalDoses.dropFirst(Int(interval.end) + 1))
                }
                
                ApiEndpoint.LOG_DEBUG("Doses for Interval \(interval.id) = \(intervalDoses)")
                
                postablePayloads.append(AdvertisementPayload(deviceId: dId, doses: intervalDoses))
            }
        }
        
        return postablePayloads

    }
    
    private static func combineAdjacent(deviceIntervals: inout [Interval]) -> [Interval] {
        // Consolidate newly adjancent intervals
        var prevIdx: Int! = 0
        var consumedIntervals: [Int: Bool] = [:]
        consumedIntervals[0] = false
        var intervalConsumed = false
        for idx in deviceIntervals.indices {
            if idx == 0 {
                consumedIntervals[deviceIntervals[idx].id] = false
                continue
            } else if deviceIntervals[prevIdx].adjacent(deviceIntervals[idx]) {
                deviceIntervals[prevIdx].add(deviceIntervals[idx])
                consumedIntervals[deviceIntervals[idx].id] = true
                intervalConsumed = true
                ApiEndpoint.LOG_DEBUG("Interval \(deviceIntervals[prevIdx].id) consumes Interval \(deviceIntervals[idx].id)")
            } else {
                prevIdx = idx
                consumedIntervals[deviceIntervals[idx].id] = false
            }
        }
        
        if intervalConsumed {
            var newIntervals: [Interval] = []
            for interval in deviceIntervals {
                if !consumedIntervals[interval.id]! {
                    newIntervals.append(interval)
                }
            }
            return combineAdjacent(deviceIntervals: &newIntervals)
        } else {
            return deviceIntervals
        }
    }
    
    // MARK: Dose Processing
    
    @Atomic static var deviceId: UInt8?
    @Atomic static var pendingPayloads: [AdvertisementPayload] = []
    @Atomic static var stagedPayloads: [AdvertisementPayload] = []
    @Atomic static var stagedPayloadsSuccessfullyUploaded: Bool = false
    @Atomic static var isScheduledForRefresh = false
    
    static func addPayload(advertisement: AdvertisementPayload?) {
        if advertisement != nil {
            ApiEndpoint.LOG_DEBUG("Waiting for PENDING_PAYLOAD_LOCK ...")
            pthread_mutex_lock(&DoseService.PENDING_PAYLOAD_LOCK)
            ApiEndpoint.LOG_DEBUG("PENDING_PAYLOAD_LOCK acquired.")
            
            // Merge the advertisement payload with the existing pending doses
            DoseService.pendingPayloads.append(advertisement!)

            pthread_mutex_unlock(&DoseService.PENDING_PAYLOAD_LOCK)
            ApiEndpoint.LOG_DEBUG("PENDING_PAYLOAD_LOCK released.")
        }
        
        // Begin uploading dosees in the foreground
        DoseService.handleMakeRequest(task: nil)

        // Scheduled delayed dispatch in case of multiple advertisements discovered in a burst
        if !DoseService.isScheduledForRefresh {
            ApiEndpoint.LOG_DEBUG("DoseService scheduling a request")
            DoseService.isScheduledForRefresh = true

            // Wait for several advertisements to coaleasce
            DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
                // If the foreground task fails, the app refresh may succeed
                if(UIApplication.shared.applicationState != UIApplication.State.active) {
                    // Schedule a background task
                    ApiEndpoint.LOG_DEBUG("Submitting BGAppRefreshTask for \(DoseService.TASK_ID)")
                    let request = BGAppRefreshTaskRequest(identifier: DoseService.TASK_ID)
                    request.earliestBeginDate = Date()
                    do {
                        try BGTaskScheduler.shared.submit(request)
                        DoseService.isScheduledForRefresh = true
                    } catch {
                        ApiEndpoint.LOG_ERROR("Could not schedule BGAppRefreshTask: \(error)")
                        DoseService.isScheduledForRefresh = false
                    }
                }
            }
        } else {
            ApiEndpoint.LOG_DEBUG("DoseService skipping scheduling a request")
        }
    }
}
