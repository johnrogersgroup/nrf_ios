//
//  HomeVC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 3/6/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard UserDefaults.standard.string(forKey: "Username") != nil else {
            return
        }

        guard UserDefaults.standard.string(forKey: "Password") != nil else {
            return
        }

//        UserDefaults.standard.set(nil, forKey: "SurveyResult")
        self.performSegue(withIdentifier: "segueFromHomeToLogin", sender: nil)
    }
}
