//
//  AppDelegate.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/11/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import UIKit
import CoreBluetooth
import CoreLocation

@propertyWrapper
struct Atomic<Value> {

    private var value: Value
    private let lock = NSLock()

    init(wrappedValue value: Value) {
        self.value = value
    }

    var wrappedValue: Value {
      get { return load() }
      set { store(newValue: newValue) }
    }

    func load() -> Value {
        lock.lock()
        defer { lock.unlock() }
        return value
    }

    mutating func store(newValue: Value) {
        lock.lock()
        defer { lock.unlock() }
        value = newValue
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CBCentralManagerDelegate, CBPeripheralDelegate, CLLocationManagerDelegate, UNUserNotificationCenterDelegate {

    let NORDIC_UART_SERVICE_UUID = CBUUID(string: "6E400001-B5A3-F393-E0A9-E50E24DCCA9E")
    let NORDIC_UART_RX_CHARACTERISTIC_UUID = CBUUID(string: "6E400002-B5A3-F393-E0A9-E50E24DCCA9E")
    let NORDIC_UART_TX_CHARACTERISTIC_UUID = CBUUID(string: "6E400003-B5A3-F393-E0A9-E50E24DCCA9E")
    
    let UV_GUARD_CENTRAL_MANAGER_IDENTIFIER = "UV_GUARD_CENTRAL_MANAGER_IDENTIFIER";

    var window: UIWindow?
    var apiEndpoint: ApiEndpoint = ApiEndpoint()
    var isScanning: Bool = false
    var centralManager: CBCentralManager!
    var locationManager: CLLocationManager!
    var deviceByDiscoveryDate: [UInt8: Date] = [:]
    static var CONNECT_LOCK = pthread_mutex_t()
    var peripherals: [UUID:CBPeripheral] = [:]
    var payloads: [UUID:Data] = [:]
    var peripheralsRx: [UUID:CBCharacteristic] = [:]
    var connectedPeripheralTaskId: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    let CONNECTED_TASK_ID: String = "com.wearifi.uv-guard.connect-peripheral"
    var app: UIApplication!

    var registeredDevices: [Device: Bool] = [:]
    var devicePeripherals: [Device: CBPeripheral?] = [:]
    var onPayloadAdvertised: [String: () -> Void]! = [:]
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        ApiEndpoint.LOG_DEBUG("Application will finish launching")
        DoseService.registerBackgroundIdentifiers()
        application.applicationIconBadgeNumber = 0
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        return true
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        ApiEndpoint.LOG_DEBUG("Received a background fetch notification")
        self.centralManager.stopScan()
        scanForUVSensor()
        DispatchQueue.main.asyncAfter(deadline: .now() + 25) {
            ApiEndpoint.LOG_DEBUG("Finished background fetch notification wait. Potentially returning to background state")
            completionHandler(UIBackgroundFetchResult.newData)
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        registerAndInit(application)
        app = application
        ApiEndpoint.LOG_DEBUG("Did finish launching with device token and options \(String(describing: launchOptions))")
        if launchOptions != nil && launchOptions![UIApplication.LaunchOptionsKey.location] != nil {
            ApiEndpoint.LOG_DEBUG("Launched due to significant location change: \(String(describing: launchOptions![UIApplication.LaunchOptionsKey.location]))")
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        ApiEndpoint.LOG_DEBUG("connectingSceneSession")
        DoseService.registerBackgroundIdentifiers()
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
        ApiEndpoint.LOG_DEBUG("didDiscardSceneSessions")
        DoseService.registerBackgroundIdentifiers()
    }
    
    // MARK: Notification Machine
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        ApiEndpoint.LOG_ERROR("Failed to register for remote notifications: \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        ApiEndpoint.LOG_DEBUG("Successfully registered for notifications!")
        
        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        // 2. Print device token to use for PNs payloads
        ApiEndpoint.LOG_DEBUG("Recording and posting Remote Notification Device Token")
        UserDefaults.standard.set(token, forKey: "DeviceToken")
        ApiEndpoint.postNotification(deviceToken: token)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for notifications: \(error.localizedDescription)")
    }
    
    func registerAndInit(_ application: UIApplication) {
        // Set defaults that may read or write on events
        if UserDefaults.standard.string(forKey: "Latitude") == nil && UserDefaults.standard.string(forKey: "Longitude") == nil {
            UserDefaults.standard.set(41.8822, forKey: "Latitude")
            UserDefaults.standard.set(-87.6220, forKey: "Longitude")
        }

        // Create the manager instances
        centralManager = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionRestoreIdentifierKey: UV_GUARD_CENTRAL_MANAGER_IDENTIFIER])
        locationManager = CLLocationManager()
        UNUserNotificationCenter.current().delegate = self
        
        // Acquire permission to get the location
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            locationManager.startMonitoringSignificantLocationChanges()
        }
        
        // Acquire permission to show the notification
        requestNotificationPermission(application)
        
        // Populate the devices for CB to scan and connect to
        ApiEndpoint.getDevices(num_doses: 0, completion: { (devices: [Device: [RecordedDose]]) in
            for (device, _) in devices {
                if self.registeredDevices[device] == nil {
                    self.registeredDevices[device] = true
                }
            }
            (UIApplication.shared.delegate as! AppDelegate).centralManagerDidUpdateState((UIApplication.shared.delegate as! AppDelegate).centralManager)
        })
    }
    
    func triggerNotificationRegistration(_ application: UIApplication) {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.removeAllDeliveredNotifications()
        notificationCenter.removeAllPendingNotificationRequests()
        
        let content = UNMutableNotificationContent()
        content.title = "UV Guard Reminder"
        content.body = "Reminder to check exposure"
        content.sound = UNNotificationSound.default

        for i in 0...1 { // go higher to hit 6pm
            var dateComponents = DateComponents()
            dateComponents.calendar = Calendar.current
            dateComponents.hour = i == 0 ? 10 : i == 1 ? 15 : 18; // 10AM, 3PM, 6PM
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
            let uuidString = UUID().uuidString
            let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
            notificationCenter.add(request) { (error) in
                if let error = error {
                    fatalError(error.localizedDescription)
                }
            }
        }
    }
    
    func requestNotificationPermission(_ application: UIApplication) {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound, .badge], completionHandler: { granted, error in
            if let error = error {
                fatalError(error.localizedDescription)
            }
            
            if !granted {
                self.requestNotificationPermission(application)
            } else {
                self.triggerNotificationRegistration(application)
            }
        })
    }
    
    
    // MARK: CoreBluetooth Delegate
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            ApiEndpoint.LOG_DEBUG("CoreBluetooth CentralManager State = unknown")
        case .resetting:
            ApiEndpoint.LOG_DEBUG("CoreBluetooth CentralManager State = resetting")
        case .unsupported:
            ApiEndpoint.LOG_DEBUG("CoreBluetooth CentralManager State = unsupported")
        case .unauthorized:
            ApiEndpoint.LOG_DEBUG("CoreBluetooth CentralManager State = unauthorized")
        case .poweredOff:
            ApiEndpoint.LOG_DEBUG("CoreBluetooth CentralManager State = poweredOff")
            centralManager.stopScan()
            isScanning = false
        case .poweredOn:
            ApiEndpoint.LOG_DEBUG("CoreBluetooth CentralManager State = poweredOn")
            let needToScan = registeredDevices.reduce(false) { (previous, entry) -> Bool in
                return previous || (devicePeripherals[entry.key] == nil)
            }
            if needToScan {
                ApiEndpoint.LOG_DEBUG("Starting scan with registeredDevices: \(registeredDevices) and devicePeripherals \(devicePeripherals)")
                scanForUVSensor()
            } else {
                ApiEndpoint.LOG_DEBUG("Skipping start scan with registeredDevices: \(registeredDevices) and devicePeripherals \(devicePeripherals)")
                self.centralManager.stopScan()
            }
            
            self.peripherals.forEach { (key: UUID, peripheral: CBPeripheral) in
                ApiEndpoint.LOG_DEBUG("Pending connection to \(peripheral)")
                self.centralManager.connect(peripheral, options: nil)
            }
        default:
            ApiEndpoint.LOG_DEBUG("CBCM UNEXPECTED UNHANDLED MANAGER STATE: \(central.state.rawValue)")
        }
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if advertisementData[CBAdvertisementDataManufacturerDataKey] != nil {
            ApiEndpoint.LOG_DEBUG("FOUND from AppDelegate \(peripheral)")
            guard let sensorData = advertisementData[CBAdvertisementDataManufacturerDataKey] as? Data else {
                ApiEndpoint.LOG_DEBUG("Failed to parse kCBAdvDataManufacturerData(\(CBAdvertisementDataManufacturerDataKey)) for \(peripheral.identifier.uuidString)")
                return
            }
            
            guard sensorData.count > 3 else {
                return
            }
            
            ApiEndpoint.LOG_DEBUG("Id Byte: \(sensorData[0]) \(sensorData[1]) Device Id Byte: \(sensorData[2]) Payload: \(sensorData.hexEncodedString())")
            
            guard sensorData[0] == 0xFF && sensorData[1] == 0xFF else {
                return
            }
            
            pthread_mutex_lock(&AppDelegate.CONNECT_LOCK);
            var doConnect = false
            var foundDevice: Device? = nil
            for (device, _) in self.registeredDevices {
                if sensorData.count == 4 && device.id == sensorData[2] {
                    foundDevice = device
                    
                    // Do connect if we haven't connected in 1 minute
                    if deviceByDiscoveryDate[sensorData[2]] != nil {
                        let components = Calendar.current.dateComponents([.minute], from: deviceByDiscoveryDate[sensorData[2]]!, to: Date())
                        if components.minute! >= 1 {
                            ApiEndpoint.LOG_DEBUG("Will connect to \(peripheral) after \(components) since last discovery")
                            doConnect = true
                            deviceByDiscoveryDate[sensorData[2]] = Date()
                        } else {
                            ApiEndpoint.LOG_DEBUG("Will **not** connect to \(peripheral) after \(components) since last discovery")
                        }
                    } else {
                        ApiEndpoint.LOG_DEBUG("Will connect to \(peripheral) after since first discovery")
                        doConnect = true
                        deviceByDiscoveryDate[sensorData[2]] = Date()
                    }
                    break
                }
            }
            pthread_mutex_unlock(&AppDelegate.CONNECT_LOCK);
            
            ApiEndpoint.LOG_DEBUG("\(String(describing: peripheral)) matches device \(String(describing: foundDevice)). Decided to doConnect = \(doConnect)")
            if foundDevice == nil || !doConnect {
                return
            }
            
            ApiEndpoint.LOG_DEBUG("Setting self.devicePeripherals[\(foundDevice!)] = \(peripheral) with current as \(String(describing: self.devicePeripherals[foundDevice!]))")
            self.devicePeripherals[foundDevice!] = peripheral
            
            // Begin a background task that may last longer than the connection to allow posting doses to start
            guard connectedPeripheralTaskId == UIBackgroundTaskIdentifier.invalid else {
                ApiEndpoint.LOG_DEBUG("Attempting to connect to another device while already connected: \(self.peripherals)")
                return
            }
            
            ApiEndpoint.LOG_DEBUG("Beginning background task with identifier: \(self.connectedPeripheralTaskId)")
            connectedPeripheralTaskId = UIApplication.shared.beginBackgroundTask(withName: CONNECTED_TASK_ID) {
                ApiEndpoint.LOG_DEBUG("Finished background task with identifier due to expiration: \(self.CONNECTED_TASK_ID) (\(self.connectedPeripheralTaskId))")
                self.connectedPeripheralTaskId = UIBackgroundTaskIdentifier.invalid
            }
            
            DispatchQueue.global().asyncAfter(deadline: .now() + 5) {
                if self.connectedPeripheralTaskId != UIBackgroundTaskIdentifier.invalid {
                    ApiEndpoint.LOG_DEBUG("Finished background task with identifier due to timeout: \(self.CONNECTED_TASK_ID) (\(self.connectedPeripheralTaskId))")
                    UIApplication.shared.endBackgroundTask(self.connectedPeripheralTaskId)
                    self.connectedPeripheralTaskId = UIBackgroundTaskIdentifier.invalid
                }
            }
                        
            self.peripherals[peripheral.identifier] = peripheral
            peripheral.delegate = self
            ApiEndpoint.LOG_DEBUG("Pending connection to \(peripheral)")
            self.centralManager.connect(peripheral, options: nil)
            self.centralManagerDidUpdateState(self.centralManager)
        }
    }
    
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        ApiEndpoint.LOG_DEBUG("didConnect peripheral: \(peripheral)")
        ApiEndpoint.LOG_DEBUG("\(peripheral) has \(String(describing: peripheral.services)) on connection")
        peripheral.delegate = self
        peripheral.discoverServices([NORDIC_UART_SERVICE_UUID])
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        ApiEndpoint.LOG_DEBUG("didDisconnectPeripheral: \(peripheral) Error: \(String(describing: error))")
        self.payloads.removeValue(forKey: peripheral.identifier)
        self.peripheralsRx.removeValue(forKey: peripheral.identifier)
        
        self.centralManagerDidUpdateState(self.centralManager)
    }

    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        ApiEndpoint.LOG_DEBUG("didFailToConnect peripheral: \(peripheral) with \(String(describing: error))")
        self.payloads.removeValue(forKey: peripheral.identifier)
        self.peripheralsRx.removeValue(forKey: peripheral.identifier)
        
        self.centralManagerDidUpdateState(self.centralManager)
    }
    
    func centralManager(_ central: CBCentralManager, willRestoreState state: [String : Any]) {
        ApiEndpoint.LOG_DEBUG("CBCentralManager is restoring state from \(state)")
        if let restoredPeripherals = state[CBCentralManagerRestoredStatePeripheralsKey] as? [CBPeripheral]? {
            for peripheral in restoredPeripherals ?? [] {
                peripheral.delegate = self
                self.peripherals[peripheral.identifier] = peripheral
            }
        }
    }
    
    func scanForUVSensor() {
        // Start looking for peripherals
        centralManager.scanForPeripherals(withServices: [NORDIC_UART_SERVICE_UUID], options:[CBCentralManagerScanOptionAllowDuplicatesKey: true])
        isScanning = true
        
        // Check if we already found some peripherals
        let peripheralIds: [UUID] = self.peripherals.map { (key: UUID, value: CBPeripheral) in
            return key
        }
        let retrievedPeripherals = centralManager.retrievePeripherals(withIdentifiers: peripheralIds)
        ApiEndpoint.LOG_DEBUG("Retrieved peripherals: \(retrievedPeripherals)")
        let connectedPeripherals = centralManager.retrieveConnectedPeripherals(withServices: [NORDIC_UART_SERVICE_UUID])
        ApiEndpoint.LOG_DEBUG("Connected peripherals: \(connectedPeripherals)")
        
        for peripheral in retrievedPeripherals {
            ApiEndpoint.LOG_DEBUG("Pending connection to \(peripheral)")
            self.centralManager.connect(peripheral, options: nil)
        }
        
        for peripheral in connectedPeripherals {
            ApiEndpoint.LOG_DEBUG("Pending connection to \(peripheral)")
            self.centralManager.connect(peripheral, options: nil)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let error = error {
            ApiEndpoint.LOG_ERROR("Failed to discover services on peripheral: \(error)")
            return
        }
        
        ApiEndpoint.LOG_DEBUG("Discovered services \(peripheral.services!) on \(peripheral)")

        for service in peripheral.services! {
            if service.uuid == NORDIC_UART_SERVICE_UUID {
                ApiEndpoint.LOG_DEBUG("Discovering Characteristics for Nordic UART on \(peripheral)")
                peripheral.discoverCharacteristics(nil, for: service)
                return
            } else {
                ApiEndpoint.LOG_ERROR("Discovered service \(service) without \(NORDIC_UART_SERVICE_UUID)")
            }
        }
        self.peripherals.removeValue(forKey: peripheral.identifier)
        self.payloads.removeValue(forKey: peripheral.identifier)
        self.peripheralsRx.removeValue(forKey: peripheral.identifier)
        ApiEndpoint.LOG_DEBUG("Cancelling connection to \(peripheral)")
        self.centralManager.cancelPeripheralConnection(peripheral)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if let error = error {
            ApiEndpoint.LOG_ERROR("Failed to discover characteristics on peripheral: \(error)")
            return
        }
        
        ApiEndpoint.LOG_DEBUG("Discovered characteristics on \(peripheral)")
        var rxChar: CBCharacteristic? = nil
        var txChar: CBCharacteristic? = nil
        for characteristic in service.characteristics! {
            if characteristic.uuid == NORDIC_UART_RX_CHARACTERISTIC_UUID {
                ApiEndpoint.LOG_DEBUG("Found NORDIC_UART_RX_CHARACTERISTIC_UUID on \(peripheral)'s \(service)")
                /*
                 The peer can send data to the device by writing to the RX Characteristic of the service.
                 ATT Write Request or ATT Write Command can be used. The received data is sent on the UART interface.
                 */
                rxChar = characteristic
            } else if characteristic.uuid == NORDIC_UART_TX_CHARACTERISTIC_UUID {
                ApiEndpoint.LOG_DEBUG("Found NORDIC_UART_TX_CHARACTERISTIC_UUID on \(peripheral)'s \(service)")
                /*
                 If the peer has enabled notifications for the TX Characteristic, the application can send data to the peer as notifications.
                 The application will transmit all data received over UART as notifications.
                 */
                txChar = characteristic
            } else {
                ApiEndpoint.LOG_ERROR("Discovered characteristic \(characteristic) without a known UUID")
            }
        }
        
        
        if rxChar == nil || txChar == nil {
            ApiEndpoint.LOG_DEBUG("\(peripheral) did not have RX and TX characterisstics. Cancelling connection with peripheral ...")
            if self.peripherals[peripheral.identifier] != nil {
                self.peripherals.removeValue(forKey: peripheral.identifier)
                self.payloads.removeValue(forKey: peripheral.identifier)
                self.peripheralsRx.removeValue(forKey: peripheral.identifier)
                ApiEndpoint.LOG_DEBUG("Cancelling connection to \(peripheral)")
                self.centralManager.cancelPeripheralConnection(peripheral)
            }
        } else {
            // Prepare to see changes to the values that we write on the sensor
            peripheral.setNotifyValue(true, for: txChar!)
            
            // Trigger the sensor to start sending data
            self.peripheralsRx[peripheral.identifier] = rxChar!
            peripheral.writeValue("DEADBEEF".data(using: String.Encoding.ascii)!, for: rxChar!, type: .withResponse)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        ApiEndpoint.LOG_DEBUG("\(peripheral) chacteristic updated for \(characteristic)")
        if let payload = characteristic.value {
            ApiEndpoint.LOG_DEBUG("Updated value had payload: \(payload.hexEncodedString())")
            
            if self.payloads[peripheral.identifier] == nil {
                self.payloads[peripheral.identifier] = characteristic.value!
            } else {
                let history: Data = self.payloads[peripheral.identifier]! + characteristic.value!
                
                if history.count != 32 {
                    ApiEndpoint.LOG_ERROR("Received bad NUS data: \(history.hexEncodedString())")
                    if self.peripherals[peripheral.identifier] != nil {
                        self.peripherals.removeValue(forKey: peripheral.identifier)
                        self.peripheralsRx.removeValue(forKey: peripheral.identifier)
                        self.payloads.removeValue(forKey: peripheral.identifier)
                        ApiEndpoint.LOG_DEBUG("Cancelling connection to \(peripheral)")
                        self.centralManager.cancelPeripheralConnection(peripheral)
                    }

                    return
                }
                
                let deviceId = UInt8(history[0])
                let foundDevice = self.devicePeripherals.reduce(false) { (previous: Bool, entry: (key: Device, value: CBPeripheral?)) -> Bool in
                    return previous || entry.key.id == deviceId
                }
                
                if !foundDevice {
                    ApiEndpoint.LOG_DEBUG("Connected to device that we are not registered to use: \(deviceId) not in \(self.devicePeripherals)")
                    if self.peripherals[peripheral.identifier] != nil {
                        self.peripherals.removeValue(forKey: peripheral.identifier)
                        self.peripheralsRx.removeValue(forKey: peripheral.identifier)
                        self.payloads.removeValue(forKey: peripheral.identifier)
                        ApiEndpoint.LOG_DEBUG("Cancelling connection to \(peripheral)")
                        self.centralManager.cancelPeripheralConnection(peripheral)
                    }
                    
                    return
                }
                
                let upperHistoryIndex = UInt8(history[1])
                let lowerHistoryIndex = UInt8(history[2])
                
                ApiEndpoint.LOG_DEBUG("Found Payload from Device \(deviceId) at History Index[\(upperHistoryIndex),\(lowerHistoryIndex)]")

                var doses: [RecordedDose] = []
                for i in 0..<14 {
                    let index: UInt8 = upperHistoryIndex &- UInt8(i)
                    var value = UInt16(history[4 + (2 * i) + 1])
                    value = value << 8
                    value = value | UInt16(history[4 + (2 * i)])
                    
                    doses.append(RecordedDose(recorded: Date(), value: Float(value), index: index))
                    ApiEndpoint.LOG_DEBUG("Found Dose: \(doses.last!)")
                    
                    if index == lowerHistoryIndex {
                        break
                    }
                }
                
                let advertisementPayload = AdvertisementPayload(deviceId: deviceId, doses: doses)
                
                // Persist the doses
                DoseService.addPayload(advertisement: advertisementPayload)
                
                // Call registered UI callbacks
                if UIApplication.shared.applicationState == UIApplication.State.active {
                    for (registration, callback) in onPayloadAdvertised {
                        ApiEndpoint.LOG_DEBUG("Firing callback for \(registration) on connected update of payload")
                        callback()
                    }
                } else {
                    ApiEndpoint.LOG_DEBUG("Not calling completions on dose advertisement due to application state not active")
                }

                // Disconnect gracefully
                if self.peripherals[peripheral.identifier] != nil {
                    self.peripheralsRx.removeValue(forKey: peripheral.identifier)
                    self.payloads.removeValue(forKey: peripheral.identifier)
                    self.centralManager.cancelPeripheralConnection(peripheral)
                }
            }
        } else {
            ApiEndpoint.LOG_DEBUG("Can't tell what changed for: \(characteristic)")
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        if let error = error {
            ApiEndpoint.LOG_ERROR("Failed to write to RX of sensor: \(error)")
            
            // Trigger the sensor to start sending data
            let rxChar = self.peripheralsRx[peripheral.identifier]!
            peripheral.writeValue("DEADBEEF".data(using: String.Encoding.ascii)!, for: rxChar, type: .withResponse)
        }
        
        ApiEndpoint.LOG_DEBUG("Successfully wrote to RX of sensor")
    }
        
    // MARK: CoreLocation Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        ApiEndpoint.LOG_DEBUG("locations = \(locValue.latitude) \(locValue.longitude)")
        UserDefaults.standard.set(locValue.latitude, forKey: "Latitude")
        UserDefaults.standard.set(locValue.longitude, forKey: "Longitude")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        ApiEndpoint.LOG_ERROR("Failed location updated with \(error)")
        
        if UserDefaults.standard.string(forKey: "Latitude") == nil {
            UserDefaults.standard.set(41.8822, forKey: "Latitude")
            UserDefaults.standard.set(-87.6220, forKey: "Longitude")
        }
    }
    
    // MARK: Notification Delegate
    
    // This method will be called when app received push notifications in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        ApiEndpoint.LOG_DEBUG("Got notification with content \(notification.request.content.body)")
        completionHandler([.alert, .badge, .sound])
        for (registration, callback) in onPayloadAdvertised {
            ApiEndpoint.LOG_DEBUG("Firing callback for \(registration) on notification for payload")
            callback()
        }
    }
}

// Global Extensions

extension UIViewController {
    func tapOutOfKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboardObjc))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboardObjc() {
        view.endEditing(true)
    }
}

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }

    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}

extension UIView {
    func bindToKeyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(UIView.keyboardWillChange(notification:)), name: UIApplication.keyboardWillChangeFrameNotification, object: nil)
    }

    func unbindToKeyboard(){
        NotificationCenter.default.removeObserver(self, name: UIApplication.keyboardWillChangeFrameNotification, object: nil)
    }
    @objc
    func keyboardWillChange(notification: Notification) {
        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let curFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let targetFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let deltaY = (targetFrame.origin.y - curFrame.origin.y) / 2

        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
            self.frame.origin.y+=deltaY

        },completion: nil)

    }
}
