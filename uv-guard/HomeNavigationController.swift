//
//  HomeNavigationController.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/11/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import UIKit

class HomeNavigationController: UINavigationController {
    
    var loggedIn: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Api
        ApiEndpoint.LOG_DEBUG("Loaded HomeNavigationController")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
}

