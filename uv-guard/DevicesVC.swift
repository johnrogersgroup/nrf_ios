//
//  DevicesVC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 3/7/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import UIKit
import CoreBluetooth
import Toast

class Device: Hashable, Equatable, Comparable, CustomStringConvertible {
    let id: UInt8
    let placement: String
    
    public var description: String {
        return "Device(\(id), \(placement)))"
    }
    
    init(id: UInt8, placement: String) {
        self.id = id
        self.placement = placement
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: Device, rhs: Device) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func < (lhs: Device, rhs: Device) -> Bool {
        return lhs.id < rhs.id;
    }

    static func <= (lhs: Device, rhs: Device) -> Bool {
        return lhs.id <= rhs.id
    }

    static func >= (lhs: Device, rhs: Device) -> Bool {
        return lhs.id >= rhs.id
    }

    static func > (lhs: Device, rhs: Device) -> Bool {
        return lhs.id >= rhs.id
    }

}

class DevicesVC: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var deviceIdTextField: UITextField!
    @IBOutlet weak var addDeviceButton: UIButton!
    
    // MARK: Edit Outlets
    @IBOutlet weak var leftWristImage: UIButton!
    @IBOutlet weak var rightWristImage: UIButton!
    
    // MARK: Existing Outlets
    @IBOutlet weak var trash1: UIButton!
    @IBOutlet weak var deviceId1: UILabel!
    @IBOutlet weak var seen1: UILabel!
    @IBOutlet weak var connect1: UIImageView!
    @IBOutlet weak var place1: UIImageView!
    @IBOutlet weak var placeLabel1: UILabel!
    
    @IBOutlet weak var trash2: UIButton!
    @IBOutlet weak var deviceId2: UILabel!
    @IBOutlet weak var seen2: UILabel!
    @IBOutlet weak var connect2: UIImageView!
    @IBOutlet weak var place2: UIImageView!
    @IBOutlet weak var placeLabel2: UILabel!

    @IBOutlet weak var trash3: UIButton!
    @IBOutlet weak var deviceId3: UILabel!
    @IBOutlet weak var seen3: UILabel!
    @IBOutlet weak var connect3: UIImageView!
    @IBOutlet weak var place3: UIImageView!
    @IBOutlet weak var placeLabel3: UILabel!


    @IBOutlet weak var trash4: UIButton!
    @IBOutlet weak var deviceId4: UILabel!
    @IBOutlet weak var seen4: UILabel!
    @IBOutlet weak var connect4: UIImageView!
    @IBOutlet weak var place4: UIImageView!
    @IBOutlet weak var placeLabel4: UILabel!

    // MARK: Stateful members
    
    var selectedPlaceImage: String?
    
    let SKIP_API: String = "SKIP_API"
    let LABEL_MAP: [String: String] = [
        "left_wrist": "Left Wrist",
        "right_wrist": "Right Wrist"
    ]
    
    var addedDevices: Int = 0
    var deviceSlots: [Device: Int] = [:]
    
    @IBAction func onDeviceIdEdit(_ sender: Any) {
        self.setAddDeviceEnabled()
    }
    
    @IBAction func selectedLeftWristImage(_ sender: Any) {
        self.selectedPlaceImage = "left_wrist"
        leftWristImage.layer.borderColor = UIColor.blue.cgColor
        rightWristImage.layer.borderColor = UIColor.secondarySystemBackground.cgColor

        self.setAddDeviceEnabled()
    }
    
    @IBAction func selectedRightWristImage(_ sender: Any) {
        self.selectedPlaceImage = "right_wrist"
        leftWristImage.layer.borderColor = UIColor.secondarySystemBackground.cgColor
        rightWristImage.layer.borderColor = UIColor.blue.cgColor

        self.setAddDeviceEnabled()
    }

    func addDevice(skipApi: Bool, index: Int, id: UInt8, placement: String) {
        ApiEndpoint.LOG_DEBUG("Displaying device \(id) in \(index)")
        self.addDevice(byId: index, forDevice: Device(id: id, placement: placement), skipApi: skipApi)
    }
    
    @IBAction func onAddDevice(_ sender: Any) {
        ApiEndpoint.LOG_DEBUG("DEVICES :: \((UIApplication.shared.delegate as! AppDelegate).registeredDevices)")
        let slotToUse: Int? = (UIApplication.shared.delegate as! AppDelegate).registeredDevices.count == 4 ? nil : (UIApplication.shared.delegate as! AppDelegate).registeredDevices.count
        guard slotToUse != nil else {
            ApiEndpoint.LOG_DEBUG("Could not add a device when all slots are taken")
            return
        }
        self.addDevice(skipApi: false, index: slotToUse!, id: UInt8(self.deviceIdTextField.text!)!, placement: self.selectedPlaceImage!)
    }
    
    @IBAction func onDeleteDevice1(_ sender: Any) {
        var skipApi: Bool = false
        if let senderMessage = sender as? String {
            skipApi = senderMessage == SKIP_API
        }
        
        let device: Device? = self.deviceSlots.reduce(nil) { (previous, entry: (key: Device, value: Int)) -> Device? in
            if entry.value == 0 {
                return entry.key
            }
            return previous
        }
        
        guard device != nil else {
            ApiEndpoint.LOG_ERROR("FATAL ERROR IN DEVICE SLOT MANAGEMENT")
            fatalError("FATAL ERROR IN DEVICE SLOT MANAGEMENT")
        }
        
        self.deleteDevice(device: device!, skipApi: skipApi, withUiCompletion: {
            self.trash1.isEnabled = false
            self.trash1.tintColor = UIColor.lightGray
            self.deviceId1.text = "-"
            self.seen1.text = "-"
            self.connect1.image = UIImage(systemName: "wifi.slash")
            self.connect1.tintColor = UIColor.darkGray
            self.place1.image = nil
            self.placeLabel1.text = ""
        })
    }
    
    @IBAction func onDeleteDevice2(_ sender: Any) {
        var skipApi: Bool = false
        if let senderMessage = sender as? String {
            skipApi = senderMessage == SKIP_API
        }
        
        let device: Device? = self.deviceSlots.reduce(nil) { (previous, entry: (key: Device, value: Int)) -> Device? in
            if entry.value == 1 {
                return entry.key
            }
            return previous
        }
        
        guard device != nil else {
            ApiEndpoint.LOG_ERROR("FATAL ERROR IN DEVICE SLOT MANAGEMENT")
            fatalError("FATAL ERROR IN DEVICE SLOT MANAGEMENT")
        }
        
        self.deleteDevice(device: device!, skipApi: skipApi, withUiCompletion: {
            self.trash2.isEnabled = false
            self.trash2.tintColor = UIColor.lightGray
            self.deviceId2.text = "-"
            self.seen2.text = "-"
            self.connect2.image = UIImage(systemName: "wifi.slash")
            self.connect2.tintColor = UIColor.darkGray
            self.place2.image = nil
            self.placeLabel2.text = ""
        })
    }
    
    @IBAction func onDeleteDevice3(_ sender: Any) {
        var skipApi: Bool = false
        if let senderMessage = sender as? String {
            skipApi = senderMessage == SKIP_API
        }
        
        let device: Device? = self.deviceSlots.reduce(nil) { (previous, entry: (key: Device, value: Int)) -> Device? in
            if entry.value == 2 {
                return entry.key
            }
            return previous
        }
        
        guard device != nil else {
            ApiEndpoint.LOG_ERROR("FATAL ERROR IN DEVICE SLOT MANAGEMENT")
            fatalError("FATAL ERROR IN DEVICE SLOT MANAGEMENT")
        }
        
        self.deleteDevice(device: device!, skipApi: skipApi, withUiCompletion: {
            self.trash3.isEnabled = false
            self.trash3.tintColor = UIColor.lightGray
            self.deviceId3.text = "-"
            self.seen3.text = "-"
            self.connect3.image = UIImage(systemName: "wifi.slash")
            self.connect3.tintColor = UIColor.darkGray
            self.place3.image = nil
            self.placeLabel3.text = ""
        })
    }
    
    @IBAction func onDeleteDevice4(_ sender: Any) {
        var skipApi: Bool = false
        if let senderMessage = sender as? String {
            skipApi = senderMessage == SKIP_API
        }
        
        let device: Device? = self.deviceSlots.reduce(nil) { (previous, entry: (key: Device, value: Int)) -> Device? in
            if entry.value == 3 {
                return entry.key
            }
            return previous
        }
        
        guard device != nil else {
            ApiEndpoint.LOG_ERROR("FATAL ERROR IN DEVICE SLOT MANAGEMENT")
            fatalError("FATAL ERROR IN DEVICE SLOT MANAGEMENT")
        }
        
        self.deleteDevice(device: device!, skipApi: skipApi, withUiCompletion: {
            self.trash4.isEnabled = false
            self.trash4.tintColor = UIColor.lightGray
            self.deviceId4.text = "-"
            self.seen4.text = "-"
            self.connect4.image = UIImage(systemName: "wifi.slash")
            self.connect4.tintColor = UIColor.darkGray
            self.place4.image = nil
            self.placeLabel4.text = ""
        })
    }
    
    // MARK: View State Machine
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tapOutOfKeyboard()
        
        self.addDeviceButton.isEnabled = false
        self.selectedPlaceImage = nil
        
        self.trash1.isEnabled = false
        self.trash1.tintColor = UIColor.lightGray
        self.trash2.isEnabled = false
        self.trash2.tintColor = UIColor.lightGray
        self.trash3.isEnabled = false
        self.trash3.tintColor = UIColor.lightGray
        self.trash4.isEnabled = false
        self.trash4.tintColor = UIColor.lightGray
        
        leftWristImage.layer.borderWidth = 2
        rightWristImage.layer.borderWidth = 2

        leftWristImage.layer.cornerRadius = 5
        rightWristImage.layer.cornerRadius = 5

        leftWristImage.layer.borderColor = UIColor.secondarySystemBackground.cgColor
        rightWristImage.layer.borderColor = UIColor.secondarySystemBackground.cgColor
    
        self.addDeviceButton.isEnabled = false
        
        let sortedDevices = Array((UIApplication.shared.delegate as! AppDelegate).registeredDevices.keys).sorted()
        for (i, device) in sortedDevices.enumerated() {
            ApiEndpoint.LOG_DEBUG("Device[\(i)] = \(device.id)")
            self.addDevice(byId: i, forDevice: device, skipApi: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (UIApplication.shared.delegate as! AppDelegate).onPayloadAdvertised["DevicesVC"] = { () -> Void in
            self.uiUpdateCallback()
        }
        self.uiUpdateCallback()
    }
    
    func uiUpdateCallback() {
        ApiEndpoint.getDevices(num_doses: 1, completion: { (devices) in
            ApiEndpoint.LOG_DEBUG("Payload triggered updated for devices: \(devices))")

            for (device, doses) in devices {
                let slot: Int? = self.deviceSlots.reduce(nil) { (previous, entry: (key: Device, value: Int)) -> Int? in
                    if entry.key.id == device.id {
                        return entry.value
                    }
                    return previous
                }
                
                if slot == nil {
                    ApiEndpoint.LOG_ERROR("No slot for deleted device: \(devices) \(self.deviceSlots)")
                    continue
                }
                
                let format = DateFormatter()
                format.timeZone = .current
                format.dateFormat = "MM-dd HH:mm"
                let image = doses.count == 0 ? UIImage(systemName: "wifi.slash") : UIImage(systemName: "wifi");
                let tintColor = doses.count == 0 ? UIColor.orange : UIColor.green;
                let seenText = doses.count == 0 ? "-" : format.string(from: doses.first!.recorded)
                
                if slot == 0 {
                    self.connect1.image = image;
                    self.connect1.tintColor = tintColor
                    self.seen1.text = seenText
                } else if slot == 1 {
                    self.connect2.image = image;
                    self.connect2.tintColor = tintColor
                    self.seen2.text = seenText
                } else if slot == 2 {
                    self.connect3.image = image;
                    self.connect3.tintColor = tintColor
                    self.seen3.text = seenText
                } else if slot == 3 {
                    self.connect4.image = image;
                    self.connect4.tintColor = tintColor
                    self.seen4.text = seenText
                } else {
                    fatalError()
                }
            }
        })
    }
    
    // MARK: Helpers
    
    func setAddDeviceEnabled() {
        if(self.deviceIdTextField.text?.count ?? 0 > 0) {
            let deviceId = Int(self.deviceIdTextField.text!)!
            if(deviceId < 0 || deviceId > 255) {
                self.view.makeToast("The device ID must be between 0 and 255")
                self.addDeviceButton.isEnabled = false
                return
            }
            
            if(self.selectedPlaceImage != nil) {
                self.addDeviceButton.isEnabled = true
            } else {
                self.addDeviceButton.isEnabled = false
            }
        } else {
            self.addDeviceButton.isEnabled = false
        }
    }
    
    func addDevice(byId id: Int, forDevice device: Device, skipApi: Bool) {
        
        self.addDevice(device: device, skipApi: skipApi, withUiCompletion: {
            // Record and look for the device
            self.deviceSlots[device] = id
            (UIApplication.shared.delegate as! AppDelegate).registeredDevices[device] = true
            if (UIApplication.shared.delegate as! AppDelegate).devicePeripherals[device] == nil {
                (UIApplication.shared.delegate as! AppDelegate).scanForUVSensor()
            }

            // Set the known fields
            if(id == 0) {
                self.trash1.isEnabled = true
                self.trash1.tintColor = UIColor.red
                self.deviceId1.text = String(device.id)
                self.seen1.text = "-"
                self.connect1.image = UIImage(systemName: "wifi.slash")
                self.connect1.tintColor = UIColor.red
                self.place1.image = UIImage(named: device.placement)
                self.placeLabel1.text = self.LABEL_MAP[device.placement]
            } else if(id == 1) {
                self.trash2.isEnabled = true
                self.trash2.tintColor = UIColor.red
                self.deviceId2.text = String(device.id)
                self.seen2.text = "-"
                self.connect2.image = UIImage(systemName: "wifi.slash")
                self.connect2.tintColor = UIColor.red
                self.place2.image = UIImage(named: device.placement)
                self.placeLabel2.text = self.LABEL_MAP[device.placement]
            } else if(id == 2) {
                self.trash3.isEnabled = true
                self.trash3.tintColor = UIColor.red
                self.deviceId3.text = String(device.id)
                self.seen3.text = "-"
                self.connect3.image = UIImage(systemName: "wifi.slash")
                self.connect3.tintColor = UIColor.red
                self.place3.image = UIImage(named: device.placement)
                self.placeLabel3.text = self.LABEL_MAP[device.placement]
            } else if(id == 3) {
                self.trash4.isEnabled = true
                self.trash4.tintColor = UIColor.red
                self.deviceId4.text = String(device.id)
                self.seen4.text = "-"
                self.connect4.image = UIImage(systemName: "wifi.slash")
                self.connect4.tintColor = UIColor.red
                self.place4.image = UIImage(named: device.placement)
                self.placeLabel4.text = self.LABEL_MAP[device.placement]
            }
            
            if !skipApi {
                ApiEndpoint.LOG_DEBUG("User added device \(device) by id \(id)")
                self.uiUpdateCallback()
            }
        })
    }
    
    func addDevice(device: Device, skipApi: Bool, withUiCompletion completion: @escaping () -> Void) {
        DispatchQueue.main.async {
            completion()
            
            guard !skipApi else {
                DispatchQueue.main.async {
                    // Clear the selection window
                    self.deviceIdTextField.text = ""
                    self.selectedPlaceImage = nil
                    self.leftWristImage.layer.borderColor = UIColor.secondarySystemBackground.cgColor
                    self.rightWristImage.layer.borderColor = UIColor.secondarySystemBackground.cgColor

                    self.setAddDeviceEnabled()
                }
                
                (UIApplication.shared.delegate as! AppDelegate).registeredDevices[device] = true
                if (UIApplication.shared.delegate as! AppDelegate).devicePeripherals[device] == nil {
                    (UIApplication.shared.delegate as! AppDelegate).scanForUVSensor()
                }

                return
            }
            
            ApiEndpoint.postDevice(device: device, completion: { (success) in
                guard success else {
                    DispatchQueue.main.async {
                        self.deviceIdTextField.text = ""
                        self.selectedPlaceImage = nil
                        self.leftWristImage.layer.borderColor = UIColor.secondarySystemBackground.cgColor
                        self.rightWristImage.layer.borderColor = UIColor.secondarySystemBackground.cgColor
                        
                        let id = self.deviceSlots[device]
                        if id == 0 {
                            self.onDeleteDevice1(self.SKIP_API)
                        } else if id == 1 {
                            self.onDeleteDevice2(self.SKIP_API)
                        } else if id == 2 {
                            self.onDeleteDevice3(self.SKIP_API)
                        } else if id == 3 {
                            self.onDeleteDevice4(self.SKIP_API)
                        } else {
                            fatalError()
                        }
                    }
                    return
                }
                
                DispatchQueue.main.async {
                    // Clear the selection window
                    self.deviceIdTextField.text = ""
                    self.selectedPlaceImage = nil
                    self.leftWristImage.layer.borderColor = UIColor.secondarySystemBackground.cgColor
                    self.rightWristImage.layer.borderColor = UIColor.secondarySystemBackground.cgColor

                    self.setAddDeviceEnabled()
                    self.view.makeToast("Added Device with ID \(device.id)")
                }
                
                self.deviceSlots = [:]
                self.trash1.isEnabled = false
                self.trash1.tintColor = UIColor.lightGray
                self.deviceId1.text = "-"
                self.seen1.text = "-"
                self.connect1.image = UIImage(systemName: "wifi.slash")
                self.connect1.tintColor = UIColor.darkGray
                self.place1.image = nil
                self.placeLabel1.text = ""
                
                self.trash2.isEnabled = false
                self.trash2.tintColor = UIColor.lightGray
                self.deviceId2.text = "-"
                self.seen2.text = "-"
                self.connect2.image = UIImage(systemName: "wifi.slash")
                self.connect2.tintColor = UIColor.darkGray
                self.place2.image = nil
                self.placeLabel2.text = ""
                
                self.trash3.isEnabled = false
                self.trash3.tintColor = UIColor.lightGray
                self.deviceId3.text = "-"
                self.seen3.text = "-"
                self.connect3.image = UIImage(systemName: "wifi.slash")
                self.connect3.tintColor = UIColor.darkGray
                self.place3.image = nil
                self.placeLabel3.text = ""
                
                self.trash4.isEnabled = false
                self.trash4.tintColor = UIColor.lightGray
                self.deviceId4.text = "-"
                self.seen4.text = "-"
                self.connect4.image = UIImage(systemName: "wifi.slash")
                self.connect4.tintColor = UIColor.darkGray
                self.place4.image = nil
                self.placeLabel4.text = ""
                
                let sortedDevices = Array((UIApplication.shared.delegate as! AppDelegate).registeredDevices.keys).sorted()
                ApiEndpoint.LOG_DEBUG("DEVICES :: \((UIApplication.shared.delegate as! AppDelegate).registeredDevices)")
                for (i, device) in sortedDevices.enumerated() {
                    self.addDevice(skipApi: true, index: i, id: device.id, placement: device.placement)
                }
                
                self.uiUpdateCallback()
            })
        }
    }
    
    func deleteDevice(device: Device, skipApi: Bool, withUiCompletion completion: @escaping () -> Void) {
        ApiEndpoint.LOG_DEBUG("User deleted device by \(device)")
        DispatchQueue.main.async {
            if !skipApi {
                ApiEndpoint.LOG_DEBUG("Deleting \(device) of \((UIApplication.shared.delegate as! AppDelegate).registeredDevices)")
                guard (UIApplication.shared.delegate as! AppDelegate).registeredDevices[device] != nil else {
                    ApiEndpoint.LOG_ERROR("A device is missing that a user manually deleted")
                    self.deviceSlots.removeValue(forKey: device)
                    return
                }
                
                ApiEndpoint.deleteDevice(byId: UInt8(device.id), resolve: {
                    DispatchQueue.main.async {
                        completion()
                    }
                    self.deviceSlots.removeValue(forKey: device)
                    
                    (UIApplication.shared.delegate as! AppDelegate).registeredDevices.removeValue(forKey: device)
                    let peripheral = (UIApplication.shared.delegate as! AppDelegate).devicePeripherals.removeValue(forKey: device)
                    if peripheral != nil && peripheral! != nil {
                        (UIApplication.shared.delegate as! AppDelegate).centralManager.cancelPeripheralConnection(peripheral!!)
                        (UIApplication.shared.delegate as! AppDelegate).peripherals.removeValue(forKey: peripheral!!.identifier)
                    }
                    
                    (UIApplication.shared.delegate as! AppDelegate).centralManagerDidUpdateState((UIApplication.shared.delegate as! AppDelegate).centralManager)
                
                    self.deviceSlots = [:]
                    self.trash1.isEnabled = false
                    self.trash1.tintColor = UIColor.lightGray
                    self.deviceId1.text = "-"
                    self.seen1.text = "-"
                    self.connect1.image = UIImage(systemName: "wifi.slash")
                    self.connect1.tintColor = UIColor.darkGray
                    self.place1.image = nil
                    self.placeLabel1.text = ""
                    
                    self.trash2.isEnabled = false
                    self.trash2.tintColor = UIColor.lightGray
                    self.deviceId2.text = "-"
                    self.seen2.text = "-"
                    self.connect2.image = UIImage(systemName: "wifi.slash")
                    self.connect2.tintColor = UIColor.darkGray
                    self.place2.image = nil
                    self.placeLabel2.text = ""
                    
                    self.trash3.isEnabled = false
                    self.trash3.tintColor = UIColor.lightGray
                    self.deviceId3.text = "-"
                    self.seen3.text = "-"
                    self.connect3.image = UIImage(systemName: "wifi.slash")
                    self.connect3.tintColor = UIColor.darkGray
                    self.place3.image = nil
                    self.placeLabel3.text = ""
                    
                    self.trash4.isEnabled = false
                    self.trash4.tintColor = UIColor.lightGray
                    self.deviceId4.text = "-"
                    self.seen4.text = "-"
                    self.connect4.image = UIImage(systemName: "wifi.slash")
                    self.connect4.tintColor = UIColor.darkGray
                    self.place4.image = nil
                    self.placeLabel4.text = ""
                    
                    let sortedDevices = Array((UIApplication.shared.delegate as! AppDelegate).registeredDevices.keys).sorted()
                    ApiEndpoint.LOG_DEBUG("DEVICES :: \((UIApplication.shared.delegate as! AppDelegate).registeredDevices)")
                    for (i, device) in sortedDevices.enumerated() {
                        self.addDevice(skipApi: true, index: i, id: device.id, placement: device.placement)
                    }
                    
                    self.uiUpdateCallback()
                })
            } else {
                ApiEndpoint.LOG_DEBUG("Skipping API during delete device")
                DispatchQueue.main.async {
                    completion()
                }
                
                (UIApplication.shared.delegate as! AppDelegate).registeredDevices.removeValue(forKey: device)
                let peripheral = (UIApplication.shared.delegate as! AppDelegate).devicePeripherals.removeValue(forKey: device)
                if peripheral != nil && peripheral! != nil {
                    (UIApplication.shared.delegate as! AppDelegate).centralManager.cancelPeripheralConnection(peripheral!!)
                }
                
                let needToScan = (UIApplication.shared.delegate as! AppDelegate).registeredDevices.reduce(false) { (previous, entry: (key: Device, value: Bool)) -> Bool in
                    return previous || (UIApplication.shared.delegate as! AppDelegate).devicePeripherals[entry.key] == nil
                }

                if !needToScan && (UIApplication.shared.delegate as! AppDelegate).isScanning {
                    (UIApplication.shared.delegate as! AppDelegate).centralManager.stopScan()
                }

                self.view.makeToast("Device with ID \(device.id) could not be added")
                self.deviceSlots.removeValue(forKey: device)
            }
        }
    }
}
