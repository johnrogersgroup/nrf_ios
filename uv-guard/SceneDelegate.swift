//
//  SceneDelegate.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/11/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        ApiEndpoint.LOG_DEBUG("sceneDidEnterBackground")
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        ApiEndpoint.LOG_DEBUG("Clearing notifications and badges during sceneDidBecomeActive")
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            ApiEndpoint.LOG_DEBUG("Cannot refresh graph because scene doesn't have app delegate instance")
            return
        }
        
        ApiEndpoint.LOG_DEBUG("Refreshing graph because scene is newly active")
        for (registration, callback) in appDelegate.onPayloadAdvertised {
            ApiEndpoint.LOG_DEBUG("Firing callback for \(registration) on active scene")
            callback()
        }
    }
}

