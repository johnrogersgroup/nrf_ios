//
//  Weather.swift
//  uv-guard
//
//  Created by Jacob Trueb on 5/27/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import Foundation

class WeatherInfo {
    
    var description: String?
    var max_uv_index: Int?
    var uv_index: Int?
    var temperature: Int?
    
    func readyToUpload() -> Bool {
        return description != nil && max_uv_index != nil && uv_index != nil && temperature != nil
    }
}

class Weather {
    
    static func populateUvIndex(weatherInfo: WeatherInfo, completion: @escaping ((WeatherInfo)->Void)) {
        //https://www.openuv.io/uvindex#

        let lat = UserDefaults.standard.float(forKey: "Latitude")
        let lon = UserDefaults.standard.float(forKey: "Longitude")
        let uviUrl = URL(string: "https://api.openuv.io/api/v1/uv?lat=\(lat)&lng=\(lon)")
        ApiEndpoint.LOG_DEBUG("Requesting UVI at \(uviUrl!)")
        var uviRequest = URLRequest(url: uviUrl!)
        uviRequest.httpMethod = "GET"
        uviRequest.allHTTPHeaderFields = ["x-access-token": "d8b3fc5fd15f4e29b85ca4518b2c3777"]
        let uviTask = URLSession.shared.dataTask(with: uviRequest) { data, response, error in
            guard let uviResponse = response as! HTTPURLResponse? else {
                ApiEndpoint.LOG_ERROR("Failed to downcast response to HTTPURLResponse after http request")
                completion(weatherInfo)
                return
            }
            
             guard error == nil else {
                ApiEndpoint.LOG_ERROR("Failed with: \(error!.localizedDescription)")
                completion(weatherInfo)
                 return
             }

             guard data != nil else {
                 ApiEndpoint.LOG_ERROR("Failed with no data received from server")
                completion(weatherInfo)
                 return
             }
            
             guard response != nil else {
                 ApiEndpoint.LOG_ERROR("Failed with nil response from server")
                completion(weatherInfo)
                 return
             }
            
            if uviResponse.statusCode == 200 {
                   do {
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        guard let result = responseJsonData["result"] as? [String: Any] else {
                            ApiEndpoint.LOG_ERROR("UVI request did not contain UVI result")
                            completion(weatherInfo)
                            return
                        }
                        
                        guard let uvValue = result["uv"] as! NSNumber? else {
                            ApiEndpoint.LOG_ERROR("Failed to convert UV Index value to string")
                            completion(weatherInfo)
                            return
                        }
                        
                        weatherInfo.uv_index = Int(truncating: uvValue)
                        
                        completion(weatherInfo)
                        
                        if weatherInfo.readyToUpload() {
                            ApiEndpoint.postWeather(weatherInfo: weatherInfo, completion: {(errorMessage) in
                                if let message = errorMessage {
                                    ApiEndpoint.LOG_ERROR("Failed to populateUvIndex: \(message)")
                                }
                            })
                        }
                   }
               } catch let error {
                    ApiEndpoint.LOG_ERROR("Getting uv index failed with invalid responses: \(error.localizedDescription)")
                completion(weatherInfo)
               }
            } else {
                do {
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        ApiEndpoint.LOG_ERROR("Failed uvi response: \(responseJsonData)")
                    }
                } catch let error {
                    ApiEndpoint.LOG_ERROR("Failed uvi response parse: \(error) :: \(String(describing: String(data: data!, encoding: String.Encoding.utf8)))")
                }
                ApiEndpoint.LOG_ERROR("Failed to make uvi request with status code \(uviResponse.statusCode)")
                completion(weatherInfo)
            }
        }
        uviTask.resume()
    }
    
    static func populateMaxUvIndex(weatherInfo: WeatherInfo, completion: @escaping ((WeatherInfo)->Void)) {
        //jwtrueb@jbmp uv-guard % curl 'http://api.openweathermap.org/data/2.5/uvi?lon=-87.65&lat=41.85&appid=2d093171e4e2e94768506e078aa643d8'
        //{"lon":-87.65,"lat":41.85,"date_iso":"2020-04-26T12:00:00Z","date":1587902400,"value":0}

        let lat = UserDefaults.standard.float(forKey: "Latitude")
        let lon = UserDefaults.standard.float(forKey: "Longitude")
        let uviUrl = URL(string: "https://api.openweathermap.org/data/2.5/uvi?lat=\(lat)&lon=\(lon)&appid=2d093171e4e2e94768506e078aa643d8")
        ApiEndpoint.LOG_DEBUG("Requesting Max UVI at \(uviUrl!)")
        var uviRequest = URLRequest(url: uviUrl!)
        uviRequest.httpMethod = "GET"
        let uviTask = URLSession.shared.dataTask(with: uviRequest) { data, response, error in
            guard let uviResponse = response as! HTTPURLResponse? else {
                ApiEndpoint.LOG_ERROR("Failed to downcast response to HTTPURLResponse after http request")
                completion(weatherInfo)
                return
            }
            
             guard error == nil else {
                ApiEndpoint.LOG_ERROR("Failed with: \(error!.localizedDescription)")
                completion(weatherInfo)
                 return
             }

             guard data != nil else {
                 ApiEndpoint.LOG_ERROR("Failed with no data received from server")
                completion(weatherInfo)
                 return
             }
            
             guard response != nil else {
                 ApiEndpoint.LOG_ERROR("Failed with nil response from server")
                completion(weatherInfo)
                 return
             }
            
            if uviResponse.statusCode == 200 {
                   do {
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        guard responseJsonData["value"] != nil else {
                            ApiEndpoint.LOG_ERROR("UVI request did not contain UVI value")
                            completion(weatherInfo)
                            return
                        }
                        
                        guard let value = responseJsonData["value"] as! NSNumber? else {
                            ApiEndpoint.LOG_ERROR("Failed to convert UV Index value to string")
                            completion(weatherInfo)
                            return
                        }
                        
                        weatherInfo.max_uv_index = Int(truncating: value)
                        
                        completion(weatherInfo)
                        
                        if weatherInfo.readyToUpload() {
                            ApiEndpoint.postWeather(weatherInfo: weatherInfo, completion: {(errorMessage) in
                                if let message = errorMessage {
                                    ApiEndpoint.LOG_ERROR("Failed to populateMaxUvIndex: \(message)")
                                }
                            })
                        }
                   }
               } catch let error {
                    ApiEndpoint.LOG_ERROR("Getting uv index failed with invalid responses: \(error.localizedDescription)")
                completion(weatherInfo)
               }
            } else {
                do {
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        ApiEndpoint.LOG_ERROR("Failed uvi response: \(responseJsonData)")
                    }
                } catch let error {
                    ApiEndpoint.LOG_ERROR("Failed uvi response parse: \(error) :: \(String(describing: String(data: data!, encoding: String.Encoding.utf8)))")
                }
                ApiEndpoint.LOG_ERROR("Failed to make uvi request with status code \(uviResponse.statusCode)")
                completion(weatherInfo)
            }
        }
        uviTask.resume()
    }
    
    static func populateWeather(weatherInfo: WeatherInfo, completion: @escaping ((WeatherInfo)->Void)) {
        //jwtrueb@jbmp uv-guard % curl 'http://api.openweathermap.org/data/2.5/weather?q=Chicago&appid=2d093171e4e2e94768506e078aa643d8&units=imperial'
        //{"coord":{"lon":-87.65,"lat":41.85},"weather":[{"id":801,"main":"Clouds","description":"few clouds","icon":"02d"}],"base":"stations","main":{"temp":60.1,"feels_like":47.71,"temp_min":57,"temp_max":63,"pressure":1020,"humidity":31},"visibility":16093,"wind":{"speed":14.99,"deg":20,"gust":31.09},"clouds":{"all":20},"dt":1587935590,"sys":{"type":1,"id":4861,"country":"US","sunrise":1587898370,"sunset":1587948209},"timezone":-18000,"id":4887398,"name":"Chicago","cod":200}%
        
        let lat = UserDefaults.standard.float(forKey: "Latitude")
        let lon = UserDefaults.standard.float(forKey: "Longitude")
        let weatherUrl = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=2d093171e4e2e94768506e078aa643d8&units=imperial")
        ApiEndpoint.LOG_DEBUG("Requesting Weather at \(weatherUrl!)")
        var weatherRequest = URLRequest(url: weatherUrl!)
        weatherRequest.httpMethod = "GET"
        let weatherTask = URLSession.shared.dataTask(with: weatherRequest) { data, response, error in
            guard let weatherResponse = response as! HTTPURLResponse? else {
                ApiEndpoint.LOG_ERROR("Failed to downcast response to HTTPURLResponse after http request")
                completion(weatherInfo)
                return
            }
            
             guard error == nil else {
                ApiEndpoint.LOG_ERROR("Failed with: \(error!.localizedDescription)")
                completion(weatherInfo)
                 return
             }

             guard data != nil else {
                 ApiEndpoint.LOG_ERROR("Failed with no data received from server")
                completion(weatherInfo)
                 return
             }
            
             guard response != nil else {
                 ApiEndpoint.LOG_ERROR("Failed with nil response from server")
                completion(weatherInfo)
                 return
             }

            if weatherResponse.statusCode == 200 {
               do {
                if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                    guard responseJsonData["weather"] != nil else {
                        ApiEndpoint.LOG_ERROR("Weather request did not contain weather value")
                        completion(weatherInfo)
                        return
                    }
                    
                    guard responseJsonData["main"] != nil else {
                        ApiEndpoint.LOG_ERROR("Weather request did not contain main temperature value")
                        completion(weatherInfo)
                        return
                    }

                    guard let weather = responseJsonData["weather"] as! [Any?]? else {
                        ApiEndpoint.LOG_ERROR("Failed to convert UV Index value to string")
                    completion(weatherInfo)
                        return
                    }
                    
                    guard let latestWeather = weather[0] as! [String: Any]? else {
                        ApiEndpoint.LOG_ERROR("Cannot cast latest weather in weather payload")
                        completion(weatherInfo)

                        return
                    }
                    
                    guard let latestDescription = latestWeather["main"] as! String? else {
                        ApiEndpoint.LOG_ERROR("Cannot cast latest weather description to string")
                        completion(weatherInfo)

                        return
                    }
                    
                    guard latestWeather["main"] != nil else {
                        ApiEndpoint.LOG_ERROR("No main weather description in latest weather")
                        completion(weatherInfo)

                        return
                    }
                    
                    guard let mainTemp = responseJsonData["main"] as! [String: Any]? else {
                        ApiEndpoint.LOG_ERROR("Failed to convert main Temperature object to map")
                        completion(weatherInfo)

                        return
                    }
                    
                    guard let temp = mainTemp["temp"] as! NSNumber? else {
                        ApiEndpoint.LOG_ERROR("No temp in main description in weather payload")
                        completion(weatherInfo)

                        return
                    }
                    
                    weatherInfo.description = latestDescription
                    weatherInfo.temperature = Int(truncating: temp)
                    
                    completion(weatherInfo)
                    
                    if weatherInfo.readyToUpload() {
                        ApiEndpoint.postWeather(weatherInfo: weatherInfo, completion: { (errorMessage) in
                            if let message = errorMessage {
                                ApiEndpoint.LOG_ERROR("Failed to populateWeather: \(message)")
                            }
                        })
                    }
                }
               } catch let error {
                    ApiEndpoint.LOG_ERROR("Getting weather failed with invalid responses: \(error.localizedDescription)")
                completion(weatherInfo)
               }
            } else {
                do {
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                        ApiEndpoint.LOG_ERROR("Failed weather response: \(responseJsonData)")
                    }
                } catch let error {
                    ApiEndpoint.LOG_ERROR("Failed weather response parse: \(error) :: \(String(describing: String(data: data!, encoding: String.Encoding.utf8)))")
                }
                ApiEndpoint.LOG_ERROR("Failed to make weather request with status code \(weatherResponse.statusCode)")
                completion(weatherInfo)
            }
        }
        weatherTask.resume()
    }

}
