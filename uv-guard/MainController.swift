//
//  MainController.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/11/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import UIKit

class MainController: UIViewController {
    
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Loaded MainController")
            
        toggleButtons()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let destination = segue.destination as? RegisterController {
//            destination.callback = loginComplete
//        }
    }
    
    func toggleButtons() {
        if let homeNC = self.navigationController as? HomeNavigationController {
            if homeNC.loggedIn {
                registerButton.isEnabled = false
                logoutButton.isEnabled = true
            } else {
                registerButton.isEnabled = true
                logoutButton.isEnabled = true

            }
        }
    }
        
    func loginComplete(loggedIn: Bool) {
        guard let homeNC = self.navigationController as? HomeNavigationController else {
            print("Failed to cast navigation controller")
            return
        }
        
        homeNC.loggedIn = true
    
        toggleButtons()
        
        self.performSegue(withIdentifier: "segueToHome", sender: self)
    }

    @IBAction func doLogout(_ sender: Any) {
        guard let homeNC = self.navigationController as? HomeNavigationController else {
            print("Failed to cast navigation controller")
            return
        }
        
        homeNC.loggedIn = false
        
        toggleButtons()
        
        UserDefaults.standard.set(nil, forKey: "Password")
        UserDefaults.standard.set(nil, forKey: "Token")
    }
    
    
}

