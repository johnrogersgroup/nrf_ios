//
//  AdvertisementPayload.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/18/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import Foundation

public class RecordedDose : Hashable, Equatable, CustomStringConvertible {
    public var description: String {
        return "Dose(\(recorded), \(value), \(String(describing: index))"
    }
    
    /**
     * A  RecordedDose performs comparison only on the values that matter. Timestamps are not known visible
     * from the sensor so they may not be used for comparisons.
     */
    public static func == (lhs: RecordedDose, rhs: RecordedDose) -> Bool {
        // Make sure values are close enough (float to int with values that typically are double digits -> quadruple digits)
        return Int(100 * lhs.value) == Int(100 * rhs.value) && lhs.index == rhs.index
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(Int(100 * value))
        hasher.combine(index)
    }
    
    var recorded: Date
    var value: Float
    var index: UInt8?
    
    public init(recorded: Date, value: Float, index: UInt8?) {
        self.recorded = recorded
        self.value = value
        self.index = index
    }
}

public class AdvertisementPayload : Equatable, CustomStringConvertible {
    public static func == (lhs: AdvertisementPayload, rhs: AdvertisementPayload) -> Bool {
        return lhs.deviceId == rhs.deviceId && lhs.doses == rhs.doses
    }
    
    
    var valid: Bool = false
    
    // MARK: Serialized Payload Format
    
    /*
     * 0) 0xDEADBEEF
     * 1) uint8_t device id
     * 2) uint8_t payload length
     * 3) [(uint8_t, uint16_t)] tuples of payload counter and dose value
     */
    
    var historyCount: UInt8 = 0
    var deviceId: UInt8 = 0
    var payloadLength: UInt8 = 0
    var doses: [RecordedDose] = []
    
    // MARK: Payload Parsing
    
    public init(deviceId: UInt8, doses: [RecordedDose]) {
        self.deviceId = deviceId
        self.doses = doses
    }
    
    init(payload data: Data) {
        /*
         ADV
         5900 // Company identifer
         1100 // UV Hist Count + 00
         
         SCAN RSP
         beef // tail end of dead beef
         5900 // Company Identifier
         05   // Device ID
         06   // Payload Values
         10   // Index
         0a46 // Value
         0f   // ...
         09fe
         0e
         0a4e
         0d
         09fa
         0c
         0a72
         0b
         09bf
         
         */
        
        // Check that we can begin to inspect the payload
//        let HEADER_PREFIX_SIZE: UInt8 = 2 + 4 + 1 + 1
        let expectedHeader = [0x59, 0x00, 0x00, 0x00, 0xbe, 0xef, 0x59, 0x00]
        let HEADER_PREFIX_SIZE = expectedHeader.count + 2
        let DEVICE_ID_IDX = HEADER_PREFIX_SIZE - 2
        let PAYLOAD_LENGTH_IDX = DEVICE_ID_IDX + 1
        if data.count < HEADER_PREFIX_SIZE {
            valid = false
            return
        }
        
        // Check that we should begin to inspect the payload
        for i in 0..<(HEADER_PREFIX_SIZE - 2) {
            if i >= data.count {
                valid = false
                return
            }
            
            if i == 2 {
                // UV HIST COUNT can change
                historyCount = data[i]
                continue
            }
            
            if data[i] != expectedHeader[i] {
                valid = false
                return
            }
        }
        
        ApiEndpoint.LOG_DEBUG("Parsing advertisement manufactureer data with matching header: \(data.hexEncodedString())")

//        if data[1] != 0x59 ||
//            data[2] != 0x00 ||
//            data[3] != 0xDE ||
//            data[4] != 0xAD ||
//            data[5] != 0xBE ||
//            data[6] != 0xEF {
//            valid = false
//            return
//        }

        // Parse a Device Id
        deviceId = data[DEVICE_ID_IDX]
        
        // Parse the Expected Number of Doses
        payloadLength = data[PAYLOAD_LENGTH_IDX]
               
        // Check if the payload is as long as expected
        if data.count > UINT8_MAX - Int32(HEADER_PREFIX_SIZE) {
            valid = false
            return
        }
        
        // HEADER + device id + payload length + num doses * 3 bytes
        if payloadLength * 3 > UInt8(UINT8_MAX) - UInt8(HEADER_PREFIX_SIZE) {
            ApiEndpoint.LOG_CLOUD("PARSE FAILED: payloadLength * 3 > UInt8(UINT8_MAX) - HEADER_PREFIX_SIZE => \(payloadLength * 3) > \(UInt8(UINT8_MAX)) - \(HEADER_PREFIX_SIZE)")
            valid = false
            return
        }
        
        if HEADER_PREFIX_SIZE + Int(payloadLength) * 3 != data.count {
            ApiEndpoint.LOG_DEBUG("A payload that matches the header did not have the correct length")
            valid = false
            return
        }
        
        // Read measurements
        valid = true
        let recorded = Date()
        var doseIndex: UInt8? = nil
        for i in 0..<payloadLength {
            let idx = Int(UInt8(HEADER_PREFIX_SIZE) + i * 3)
            let index: UInt8 = data[idx]
            if(doseIndex != nil && doseIndex! - 1 != index) {
                ApiEndpoint.LOG_ERROR("Dose indexes are not consecutive \(doseIndex!) - 1 != \(index)!!")
                valid = false
            }
            doseIndex = index
            var value: UInt16 = UInt16(data[idx + 1])
            value = value << 8
            value = value | UInt16(data[idx + 2])
            doses.append(RecordedDose(recorded: recorded, value: Float(value), index: index))
            ApiEndpoint.LOG_DEBUG("Found Dose: \(doses.last!)")
        }
    }
    
    // MARK: Data Access
    
    func isValid() -> Bool {
        return valid
    }
    
    public func getDoses() -> [RecordedDose] {
        return doses
    }
    
    public func getDeviceId() -> UInt8 {
        return deviceId
    }
    
    public func getHistoryCount() -> UInt8 {
        return historyCount
    }
        
    public var description: String {
        return "Device Id: \(deviceId) Doses: \(doses)"
    }
}
