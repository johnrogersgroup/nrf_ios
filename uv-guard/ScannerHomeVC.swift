//
//  ScannerHomeVC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 3/6/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import UIKit
import Charts
import CoreBluetooth

class ScannerHomeVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var uvIndexLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var lineChart: LineChartView!
    @IBOutlet weak var dayPicker: UIPickerView!
    @IBOutlet weak var mostRecentLabel: UILabel!
    
    var mPickerData: [String] = [String]()
    var weatherInfo: WeatherInfo = WeatherInfo()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mPickerData = ["Today", "Yesterday"]
        dayPicker.delegate = self
        dayPicker.dataSource = self
        dayPicker.selectRow(0, inComponent: 0, animated: true)
                
        lineChart.isUserInteractionEnabled = false
        lineChart.dragEnabled = false
        lineChart.noDataText = "Loading Data ..."
            
        self.lineChart.noDataText = "No data for date selected ..."
        self.mostRecentLabel.isHidden = true
    }
    
    func fetchGraph(recurse: Bool) {
        ApiEndpoint.LOG_TRACE("Fetching new graph content ...")

        var range: String?
        if self.dayPicker.selectedRow(inComponent: 0) == 0 {
            range = "today"
        } else if self.dayPicker.selectedRow(inComponent: 0) == 1 {
            range = "yesterday"
        } else if self.dayPicker.selectedRow(inComponent: 0) == 2 {
            range = "week"
        } else {
            fatalError()
        }
        
        // Poll the latest doses if the app is in the foreground
//        if UIApplication.shared.applicationState == UIApplication.State.active {
            ApiEndpoint.getDoses(range: range, completion: { (errorMessage: String?, maybeDoses: [RecordedDose]?) in
                if let message = errorMessage {
                    ApiEndpoint.LOG_ERROR("Failed to getDoses for fetchGraph: \(message)")
                }
                
                guard let doses = maybeDoses else {
                    let delay: TimeInterval = 15
                    ApiEndpoint.LOG_ERROR("Doses are empty without error. Scheduling retry in \(delay) seconds")
                    
                    if recurse {
                        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { self.fetchGraph(recurse: recurse) }
                    }
                    
                    return
                }
                
                if doses.count > 0 {
                    ApiEndpoint.LOG_TRACE("Found \(doses.count) doses: [\(doses.first!) ... \(doses.last!)]")
                    DispatchQueue.main.async {
                        self.updateGraph(chart: self.lineChart, doses: doses)
                    }
                }
            })
//        } else {
//            ApiEndpoint.LOG_DEBUG("Electing to skip getDoses in fetchGraph because applicationState is not active")
//        }

        if recurse {
            DispatchQueue.main.asyncAfter(deadline: .now() + 120) { self.fetchGraph(recurse: true) }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.fetchGraph(recurse: true)

        (UIApplication.shared.delegate as! AppDelegate).onPayloadAdvertised["ScannerHomeVC"] = { () -> Void in
            self.fetchGraph(recurse: false)
        }
        
        populateUvIndex()
        populateMaxUvIndex()
        populateWeather()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        (UIApplication.shared.delegate as! AppDelegate).onPayloadAdvertised.removeValue(forKey: "ScannerHomeVC")
    }
    
    func populateUvIndex() {
        Weather.populateUvIndex(weatherInfo: self.weatherInfo) { (weatherInfo: WeatherInfo)  in
            ApiEndpoint.LOG_DEBUG("UV Index is populated. Updating UI ...")
            DispatchQueue.main.async {
                if let uv_index = weatherInfo.uv_index {
                    self.uvIndexLabel.text = uv_index.description
                    if weatherInfo.readyToUpload() {
                        self.weatherInfo = WeatherInfo()
                    }
                }
            }
        }
    }
    
    func populateMaxUvIndex() {
        Weather.populateMaxUvIndex(weatherInfo: self.weatherInfo) { (weatherInfo: WeatherInfo)  in
            ApiEndpoint.LOG_DEBUG("Max UV Index is populated.")
            if weatherInfo.readyToUpload() {
                self.weatherInfo = WeatherInfo()
            }
        }
    }
    
    func populateWeather() {
        Weather.populateWeather(weatherInfo: self.weatherInfo) { (weatherInfo: WeatherInfo) in
            ApiEndpoint.LOG_DEBUG("Weather is populated. Updating UI ...")
            DispatchQueue.main.async {
                if let description = weatherInfo.description {
                    if let temperature = weatherInfo.temperature {
                        self.weatherLabel.text = description
                        self.temperatureLabel.text = temperature.description
                        if weatherInfo.readyToUpload() {
                            self.weatherInfo = WeatherInfo()
                        }
                    }
                }
            }
        }
    }
    
    func computeChartValues(recordedDoses: [RecordedDose], skinScale: Double) -> ([Int: String], Int, [Double], [Double]) {
        /*

        Fitzpatrick Type    Skin color    Race    UVB MED (J/m2)    % MED Resolution    Equation Conversionm
        I    bright white    European / British    200    12.38    100% * (.0254*(high payload - low payload)) / 200
        II    white    European / Scandinavian    250    9.91    100% * (.0254*(high payload - low payload)) / 250
        III    fair    Southern or Central European    300    8.26    100% * (.0254*(high payload - low payload)) / 300
        IV    light    Mediterannean, Asian, or Latino    400    6.19    100% * (.0254*(high payload - low payload)) / 400
        V    brown    East Indian, Native American, Latino or African    600    4.13    100% * (.0254*(high payload - low payload)) / 600

        */
        
        // Compute the timestamps
        var labelValues: [Int: String] = [:]
        var burnLines: Int = 0
        var xdat: [Double] = []
        var ydat: [Double] = []
        
        if recordedDoses.count < 1 {
            return (labelValues, burnLines, xdat, ydat)
        }
        
//        // Artificially separate close doses by 15 minutes
//        for i in 0..<recordedDoses.count-1 {
//            let firstDoseDate = recordedDoses[i].created
//            let secondDoseDate = recordedDoses[i+1].created
//            let dateComponents = Calendar.current.dateComponents([.minute], from: firstDoseDate, to: secondDoseDate)
//            if dateComponents.minute! < 15 {
//                recordedDoses[i].created = secondDoseDate - (15 * 60)
//            }
//        }
        
        let initialDay: Date = Calendar.current.startOfDay(for: recordedDoses[0].recorded)
        var prevDay = initialDay
        
        // Put space between doses that are too close together
        for i in 0..<recordedDoses.count-1 {
            // Build up spacedDoses backwards due to traversal order
            // Insert a dose if the next dose is hours away
            let firstDoseIndex = recordedDoses.count - 2 - i
            let secondDoseIndex = firstDoseIndex + 1
            let firstDoseDate = recordedDoses[firstDoseIndex].recorded
            let secondDoseDate = recordedDoses[secondDoseIndex].recorded
            let dateComponents = Calendar.current.dateComponents([.minute], from: firstDoseDate, to: secondDoseDate)
            
            // Adjust the first dose since going right to left
            if dateComponents.minute! < 5 {
                recordedDoses[firstDoseIndex].recorded = Date(timeInterval: TimeInterval(-5 * 60), since: secondDoseDate)
            }
        }

        // Insert doses in between actual doses
        var graphableDoses: [RecordedDose] = []
        for i in 0..<recordedDoses.count {
            // Put simple data at the beginning of the day
            if graphableDoses.count == 0 {
//                var hoursSinceStart: Float = Float(Calendar.current.dateComponents([.hour], from: recordedDoses[i].recorded).hour!)
//
//                // Add data starting at 6
//                graphableDoses.append(RecordedDose(recorded: Date(timeInterval: TimeInterval(6 * 3600), since: initialDay), value: 0, index: nil))
//                hoursSinceStart = hoursSinceStart - 6.0
//
//                let hoursToAdd = hoursSinceStart / 2.0
//                let valueToAdd = (recordedDoses[i].value / 4.0)
//                recordedDoses[i].value -= valueToAdd
//                graphableDoses.append(RecordedDose(recorded: Date(timeInterval: TimeInterval(hoursToAdd * 3600), since: graphableDoses[0].recorded), value: valueToAdd, index: nil))
                
                
                // Add a dose 15 minutes before the first dose
                graphableDoses.append(RecordedDose(recorded: Date(timeInterval: TimeInterval(-15 * 60), since: recordedDoses[0].recorded), value: 0, index: nil))
            }
            
            // Graph the dose under inspection
            graphableDoses.append(recordedDoses[i])
            
            // Finish if there are no more doses after this
            if i + 1 == recordedDoses.count {
                break
            }
            
            // Insert a dose if the next dose is hours away
            let firstDoseDate = recordedDoses[i].recorded
            let secondDoseDate = recordedDoses[i+1].recorded
            let dateComponents = Calendar.current.dateComponents([.hour], from: firstDoseDate, to: secondDoseDate)
            if dateComponents.hour! > 4 {
                let hoursToAdd = dateComponents.hour! / 2
                let valueToAdd = (recordedDoses[i+1].value / 4.0)
                recordedDoses[i+1].value -= valueToAdd
                graphableDoses.append(RecordedDose(recorded: Date(timeInterval: TimeInterval(hoursToAdd * 3600), since: firstDoseDate), value: valueToAdd, index: nil))
            }
        }
        
        // Format the labels based on the times that are chosen
        let initialFormat = DateFormatter()
        initialFormat.timeZone = .current
        initialFormat.dateFormat = "MM-dd HH:mm"
        
        let format = DateFormatter()
        format.timeZone = .current
        format.dateFormat = "HH:mm"
        
        for i in 0..<graphableDoses.count {
            if Calendar.current.dateComponents([.day], from: prevDay, to: graphableDoses[i].recorded).day! >= 1 {
                prevDay = Calendar.current.startOfDay(for: graphableDoses[i].recorded)
            }
            
            // Compute x data
            let xComps = Calendar.current.dateComponents([.hour], from: initialDay, to: graphableDoses[i].recorded)
            var xi: Double = Double(xComps.hour!)
            xi += Double(Calendar.current.dateComponents([.minute], from: graphableDoses[i].recorded).minute!) / 60.0
            xdat.append(xi)
            
            // Compute y data
            ydat.append(Double(graphableDoses[i].value) / skinScale * 100 + (ydat.last ?? 0))
        }
        
        if Calendar.current.dateComponents([.day], from: initialDay, to: prevDay).day! == 0 {
            // Create stamps over 1 day with 1 burn line
            let hoursRepresented = Calendar.current.dateComponents([.hour], from: initialDay, to: graphableDoses.last!.recorded).hour!
            burnLines = 1
            for i in 3..<11 {
                // Label past current by 2 hours
                if (i - 2) * 2 > hoursRepresented {
                    break
                }
                
                let labelDate = Calendar.current.date(byAdding: .hour, value: i * 2, to: initialDay)
                labelValues[i*2] = i == 3 ? initialFormat.string(from: labelDate!) : format.string(from: labelDate!)
            }
        } else {
            let daysRepresented = Calendar.current.dateComponents([.day], from: initialDay, to: graphableDoses.last!.recorded).day! + 1
            burnLines = Int(daysRepresented)
            var prevDay: Date = initialDay
            for i in 1..<10 {
                // Label past current by 2 hours
                if (i - 3) * 6 > daysRepresented {
                    break
                }
                
                let labelDate = Calendar.current.date(byAdding: .hour, value: i * 6, to: initialDay)
                if i != 1 && Calendar.current.dateComponents([.day], from: prevDay, to: graphableDoses.last!.recorded).day! < 1 {
                    labelValues[i*6] = format.string(from: labelDate!)
                } else {
                    labelValues[i*6] = initialFormat.string(from: labelDate!)
                    prevDay = labelDate!
                }
            }
        }
        
        for i in 0..<xdat.count {
            ApiEndpoint.LOG_TRACE("X: \(xdat[i]), Y: \(ydat[i])")
        }

        return (labelValues, burnLines, xdat, ydat)
    }
    
    func updateGraph(chart: LineChartView, doses: [RecordedDose]) {
        var chartEntries = [ChartDataEntry]()
        
        let surveyClass = UserDefaults.standard.integer(forKey: "SurveyClass")
        var limit: Double = 200
        if surveyClass == 0 {
            limit = 200
        } else if surveyClass == 1 {
            limit = 250
        } else if surveyClass == 2 {
            limit = 300
        } else if surveyClass == 3 {
            limit = 400
        } else if surveyClass == 4 {
            limit = 600
        }
        
        let (labelValues, burnLines, xdat, ydat) = computeChartValues(recordedDoses: doses, skinScale: limit)
        var max_y: Double = 0
        for i in 0..<xdat.count {
            chartEntries.append(ChartDataEntry(x: xdat[i], y: ydat[i]))
            max_y = ydat[i]
        }

        let set1 = LineChartDataSet(entries: chartEntries, label: "Doses")
        set1.mode = LineChartDataSet.Mode.horizontalBezier
        set1.cubicIntensity = 0.1
        set1.axisDependency = .left
        set1.setColor(UIColor.systemYellow)
        set1.drawCirclesEnabled = false
        set1.lineWidth = 2
        set1.circleRadius = 3
        set1.fillAlpha = 1
        set1.drawFilledEnabled = true
        set1.fillColor = .systemBlue
        set1.highlightColor = UIColor(red: 244/255, green: 117/255, blue: 117/255, alpha: 1)
        set1.drawCircleHoleEnabled = false

        let gradientColors = [UIColor.white.cgColor, UIColor.systemBlue.cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90)
        set1.drawFilledEnabled = true

        let data = LineChartData(dataSets: [set1])
        data.setDrawValues(false)

        let scale_max = max(100.0, max_y) + 25
        
        chart.data = data
        chart.leftAxis.axisMinimum = 0
        chart.leftAxis.axisMaximum = scale_max
        chart.rightAxis.enabled = false
        
        let renderer = XAxisTimeRenderer(viewPortHandler: chart.viewPortHandler, xAxis: chart.xAxis, transformer: chart.getTransformer(forAxis: .left), labelValues: labelValues)
        chart.xAxisRenderer = renderer
        chart.xAxis.axisMinimum = 6
        chart.xAxis.axisMaximum = Double(24 * burnLines) - 1
        chart.xAxis.labelRotationAngle = 45.0
        chart.xAxis.labelCount = renderer.labelValues.count
        chart.xAxis.forceLabelsEnabled = true
        chart.xAxis.granularityEnabled = true
        chart.xAxis.granularity = 1
        chart.xAxis.valueFormatter = TimeChartFormatter(labelValues: labelValues)
        chart.xAxis.labelPosition = XAxis.LabelPosition.bottom

        
        let burnLimit = Double(burnLines) * 100.0
        let burnLabel = burnLines == 1 ? "Burn Line" : "\(burnLines) Day Burn Line"
        chart.leftAxis.addLimitLine(ChartLimitLine(limit: burnLimit, label: burnLabel))
        chart.extraBottomOffset = 40.0
        chart.extraLeftOffset = 10.0
        chart.extraRightOffset = 10.0
        
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.dateFormat = "MM-dd HH:mm"
        let recentDoseText = formatter.string(from: doses.last!.recorded)
        self.mostRecentLabel.text = "Most Recent: \(recentDoseText)"
        self.mostRecentLabel.isHidden = false
    }

    // MARK: Picker
    
       override func didReceiveMemoryWarning() {
           super.didReceiveMemoryWarning()
           // Dispose of any resources that can be recreated.
       }
    
       // Number of columns of data
       func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }
       
       // The number of rows of data
       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
           return mPickerData.count
       }
       
       // The data to return for the row and component (column) that's being passed in
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
           return mPickerData[row]
       }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        lineChart.noDataText = "Loading Data ..."
        lineChart.data = nil
        if row == 0 {
            ApiEndpoint.getDoses(range: "today", completion: { (errorMessage: String?, maybeDoses: [RecordedDose]?) in
                if let message = errorMessage {
                    ApiEndpoint.LOG_ERROR("Failed to getDoses for today: \(message)")
                    return
                }
                
                guard let doses = maybeDoses else {
                    ApiEndpoint.LOG_ERROR("Doses are empty without error")
                    return
                }
                
                if doses.count > 0 {
                    self.updateGraph(chart: self.lineChart, doses: doses)
                } else {
                    self.lineChart.noDataText = "No data for date selected ..."
                    self.lineChart.data = nil
                    self.mostRecentLabel.isHidden = true
                }
            })
        } else if row == 1 {
            ApiEndpoint.getDoses(range: "yesterday", completion: { (errorMessage: String?, maybeDoses: [RecordedDose]?) in
                if let message = errorMessage {
                    ApiEndpoint.LOG_ERROR("Failed to getDoses for yesterday: \(message)")
                    return
                }
                
                guard let doses = maybeDoses else {
                    ApiEndpoint.LOG_ERROR("Doses are empty without error")
                    return
                }
                
                if doses.count > 0 {
                    self.updateGraph(chart: self.lineChart, doses: doses)
                } else {
                    self.lineChart.noDataText = "No data for date selected ..."
                    self.lineChart.data = nil
                    self.mostRecentLabel.isHidden = true
                }
            })
        } else if row == 2 {
            ApiEndpoint.getDoses(range: "week", completion: { (errorMessage: String?, maybeDoses: [RecordedDose]?) in
                if let message = errorMessage {
                    ApiEndpoint.LOG_ERROR("Failed to getDoses for week: \(message)")
                    return
                }
                
                guard let doses = maybeDoses else {
                    ApiEndpoint.LOG_ERROR("Doses are empty without error")
                    return
                }
                
                if doses.count > 0 {
                    self.updateGraph(chart: self.lineChart, doses: doses)
                } else {
                    self.lineChart.noDataText = "No data for date selected ..."
                    self.lineChart.data = nil
                    self.mostRecentLabel.isHidden = true
                }
            })
        }
    }
}

class XAxisTimeRenderer: XAxisRenderer {
    var labelValues: [Double] = []
    
    init(viewPortHandler: ViewPortHandler, xAxis: XAxis?, transformer: Transformer?, labelValues: [Int: String]) {
        super.init(viewPortHandler: viewPortHandler, xAxis: xAxis, transformer: transformer)
        self.labelValues = []
        
        let keys = labelValues.keys.sorted()
        for i in 0..<keys.count {
            self.labelValues.append(Double(keys[i]))
        }
    }
    
    override func computeAxis(min: Double, max: Double, inverted: Bool) {
        axis?.entries = self.labelValues
    }
}

class TimeChartFormatter: NSObject, IAxisValueFormatter {
    var labelValues: [Int: String] = [:]
    
    init(labelValues: [Int: String]) {
        self.labelValues = labelValues
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        if nil != self.labelValues[Int(value)] {
            return self.labelValues[Int(value)]!
        } else {
            return ""
        }
    }
}


