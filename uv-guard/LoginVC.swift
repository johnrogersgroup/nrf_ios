//
//  LoginVC.swift
//  uv-guard
//
//  Created by Jacob Trueb on 3/5/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//


import UIKit

class LoginVC: UIViewController {

    
    @IBOutlet weak var loginStack: UIStackView!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.view.hideToastActivity()
        usernameTextField.text = UserDefaults.standard.value(forKey: "Username") as? String
        usernameTextField.delegate = self
        passwordTextField.text = UserDefaults.standard.value(forKey: "Password") as? String
        passwordTextField.delegate = self

        self.tapOutOfKeyboard()

        guard UserDefaults.standard.string(forKey: "Username") != nil else {
            return
        }
        
        guard UserDefaults.standard.string(forKey: "Password") != nil else {
            return
        }
        
        self.doLogin("placeholder")
    }

    @IBAction func stopSurveyUnwind(_ unwindSegue: UIStoryboardSegue) {
        var surveyResult: [String: String] = [:]

        surveyResult["Gender"] = UserDefaults.standard.string(forKey: "Gender")
        surveyResult["Age"] = UserDefaults.standard.string(forKey: "Age")
        surveyResult["Race"] = UserDefaults.standard.string(forKey: "Race")
        surveyResult["Hispanic"] = UserDefaults.standard.string(forKey: "Hispanic")
        surveyResult["SkinColor"] = UserDefaults.standard.string(forKey: "SkinColor")
        surveyResult["Moles"] = UserDefaults.standard.string(forKey: "Moles")
        surveyResult["SkinCancer1"] = UserDefaults.standard.string(forKey: "SkinCancer1")
        surveyResult["SkinCancer2"] = UserDefaults.standard.string(forKey: "SkinCancer2")
        surveyResult["Sunburn"] = UserDefaults.standard.string(forKey: "Sunburn")
        surveyResult["Tranning"] = UserDefaults.standard.string(forKey: "Tanning")
        
        
        UserDefaults.standard.set(surveyResult, forKey: "SurveyResult")
        
        postSurvey()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.performSegue(withIdentifier: "segueFromLoginToHome", sender: self)
        }
    }
    
    
    @IBAction func doLogin(_ sender: Any) {
        self.navigationController?.view.makeToastActivity(.center)

        sendTokenAuthRequest(username: usernameTextField.text!, password: passwordTextField.text!)
    }
    
    func postSurvey() {
        guard let surveyResults = UserDefaults.standard.value(forKey: "SurveyResult") as! [String: String]? else {
            ApiEndpoint.LOG_ERROR("Failed to convert survey result")
            return
        }
        
        /*
        
        Fitzpatrick Type    Skin color    Race    UVB MED (J/m2)    % MED Resolution    Equation Conversionm
        I    bright white    European / British    200    12.38    100% * (.0254*(high payload - low payload)) / 200
        II    white    European / Scandinavian    250    9.91    100% * (.0254*(high payload - low payload)) / 250
        III    fair    Southern or Central European    300    8.26    100% * (.0254*(high payload - low payload)) / 300
        IV    light    Mediterannean, Asian, or Latino    400    6.19    100% * (.0254*(high payload - low payload)) / 400
        V    brown    East Indian, Native American, Latino or African    600    4.13    100% * (.0254*(high payload - low payload)) / 600
 
        */
        
        guard let skinColor = UserDefaults.standard.string(forKey: "SkinColor") else {
            ApiEndpoint.LOG_ERROR("Finished survey without skin color")
            return
        }
        
        var surveyClass = 0
        if skinColor == "bright white" {
            surveyClass = 0
        } else if skinColor == "white" {
            surveyClass = 1
        } else if skinColor == "fair" {
            surveyClass = 2
        } else if skinColor == "light" {
            surveyClass = 3
        } else if skinColor == "brown" {
            surveyClass = 4
        }
        UserDefaults.standard.set(surveyClass, forKey: "SurveyClass")
        
        let _ = surveyResults.description
        
//        ApiEndpoint.postSurvey(surveyClass: NSNumber(value: surveyClass), surveyDescription: surveyDescription)
    }
    
    func sendTokenAuthRequest(username: String, password: String) {
        ApiEndpoint.makeRequest(route: "auth/", method: "POST", headers: ["Content-Type": "application/json"], body: ["username": username, "password": password], callback: { (failureMessage, error, data, httpResponse) in
            guard failureMessage == nil else {
                self.failWithMessage(message: failureMessage!)
                return
            }
            guard error == nil else {
                self.failWithMessage(message: "Failed to login with: \(error!.localizedDescription)")
                return
            }

            guard let data = data else {
                self.failWithMessage(message: "Failed to login with empty response from server")
                return
            }
            
            guard let httpResponse = httpResponse else {
                self.failWithMessage(message: "Failed to register with invalid response from server")
                return
            }
            
            if httpResponse.statusCode == 200 {
                do {
                    if let responseJsonData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        guard let token = responseJsonData["token"] else {
                            self.failWithMessage(message: "Authentication succeeded without an access token. Please contact support.")
                            return
                        }
                        UserDefaults.standard.set(token, forKey: "Token")
                        UserDefaults.standard.set(username, forKey: "Username")
                        UserDefaults.standard.set(password, forKey: "Password")
                        DispatchQueue.main.async {
                            self.navigationController?.view.hideToastActivity()
                            
                            // TODO: Make request to API to find out if survey is complete
                            let surveyResult = UserDefaults.standard.value(forKey: "SurveyResult")
                            let surveyClass = UserDefaults.standard.value(forKey: "SurveyClass")
                            let forceSurvey = false
                            DispatchQueue.main.async {
                                (UIApplication.shared.delegate as! AppDelegate).app.registerForRemoteNotifications()
                            }
                            ApiEndpoint.getDevices(num_doses: 0, completion: { (devices: [Device: [RecordedDose]]) in
                                for (device, _) in devices {
                                    if (UIApplication.shared.delegate as! AppDelegate).registeredDevices[device] == nil {
                                        (UIApplication.shared.delegate as! AppDelegate).registeredDevices[device] = true
                                    }
                                }
                                (UIApplication.shared.delegate as! AppDelegate).centralManagerDidUpdateState((UIApplication.shared.delegate as! AppDelegate).centralManager)
                            })
                            if !forceSurvey && surveyResult != nil && surveyClass != nil {
                                self.postSurvey()
                                self.performSegue(withIdentifier: "segueFromLoginToHome", sender: self)
                                ApiEndpoint.LOG_DEBUG("Proceeding to home with survey result")
                            } else {
                                self.performSegue(withIdentifier: "segueFromLoginToRegistrationSurvey", sender: self)
                            }
                        }
                    }
                } catch let error {
                    self.failWithMessage(message: "Authentication failed with invalid responses: \(error.localizedDescription)")
                }
            } else {
                self.failWithMessage(message: "Token authentication failed with code \(httpResponse.statusCode). \(self.getJsonMessage(data))")
                UserDefaults.standard.set(nil, forKey: "Username")
                UserDefaults.standard.set(nil, forKey: "Password")
            }
        })
    }
    
    func printJsonPayload(_ jsonData: Data) {
        do {
            if let responseJsonData = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any] {
                ApiEndpoint.LOG_TRACE("Received response with payload \(responseJsonData)")
            }
        } catch let error {
            ApiEndpoint.LOG_TRACE("Encountered error parsing json response data: \(error.localizedDescription)")
        }

    }
    
    func getJsonMessage(_ jsonData: Data) -> String {
        do {
            if let responseJsonData = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any] {
                ApiEndpoint.LOG_TRACE("Received response with payload \(responseJsonData)")
                if let message = responseJsonData["message"] as? String {
                    return message
                }
            }
        } catch let error {
            print("Encountered error parsing json response data: \(error.localizedDescription)")
        }
        return ""
    }
    
    func failWithMessage(message: String) {
        ApiEndpoint.LOG_ERROR(message)
        DispatchQueue.main.async {
            self.navigationController?.view.hideToastActivity()
            self.navigationController?.view.makeToast("There was a problem logging in: \(message)", position: .center)
        }
    }
}

extension LoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

