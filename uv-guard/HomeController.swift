//
//  HomeController.swift
//  uv-guard
//
//  Created by Jacob Trueb on 1/11/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import Foundation
import UIKit

class HomeController: UIViewController {
    
    @IBOutlet weak var welcomeLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        imageView.image = UIImage(named: "ic_launcher-1024-83.5")
        assert(imageView.image != nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func unwindToHomeVC(segue: UIStoryboard) {
        
    }
}
