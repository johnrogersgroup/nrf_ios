//
//  AdvertisementPayloadsTests.swift
//  AdvertisementPayloadsTests
//
//  Created by Jacob Trueb on 3/1/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import XCTest
@testable import uv_guard

class AdvertisementPayloadsTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCanParseValidPayload() {
        // The magic number that should prefix our payloads
        let _: UInt32 = 0xDEADBEEF
        
        // The device that measured the dose reading
        let deviceId: UInt16 = 0x69
        
        // The length of the dose readings in canon format
        let numReadings: UInt8 = 50
        let payloadLength: UInt8 = (1 + 2) * numReadings
        
        // The raw bytes received as the manufacturer data in BLE advertisement
        var canonBytes: [UInt8] = [
            // MAGIC NUMBER
            0xDE,
            0xAD,
            0xBE,
            0xEF,
            // DEVICE ID
            UInt8(deviceId),
            // PAYLOAD LENGTH
            UInt8(payloadLength)
            // COUNTER + DOSE PAIRS
            // ...
        ]
        
        // The values for the readings that should be parsed from the payload
        let valueStep: UInt16 = UInt16(UINT16_MAX) / UInt16(numReadings)
        for i in 1...numReadings {
            let value: UInt16 = UInt16(i) * valueStep
            canonBytes.append(UInt8(i))
            canonBytes.append(UInt8(value >> 8))
            canonBytes.append(UInt8(value & 0xFF))
        }
        
        // Parse the payload
        let payload: AdvertisementPayload = AdvertisementPayload(payload: Data(bytes: &canonBytes, count: canonBytes.count))
        XCTAssertTrue(payload.isValid())
        
        // Verify that the measurements were correctly recovered
        let doses: [RecordedDose] = payload.getDoses()
        XCTAssertEqual(Int(numReadings), doses.count)
        for i in 1...numReadings {
            print(i)
            let value: UInt16 = UInt16(i) * valueStep
            let expectedDose: RecordedDose = RecordedDose(recorded: Date(), value: Float(value), index: i)
            XCTAssertEqual(expectedDose, doses[Int(i - 1)])
        }
    }
    
    func testCanHandleInvalidPayloads() {
        var payloads: [[UInt8]] = []
        
        // Too short
        payloads.append([])
        payloads.append([1])
        payloads.append([1,2])
        payloads.append([1,2,3])
        payloads.append([1,2,3,4])
        payloads.append([1,2,3,4,5])
        
        // Invalid magic byte
        payloads.append([
           0xBE,
           0xEE,
           0xEE,
           0xEF,
           1,
           3,
           0,
           0xFF,
           0xFF
        ])
        
        // Invalid payload length
         payloads.append([
            0xDE,
            0xAD,
            0xBE,
            0xEF,
            1,
            2,
            0,
            0xFF,
            0xFF
        ])
         payloads.append([
            0xDE,
            0xAD,
            0xBE,
            0xEF,
            1,
            4,
            0,
            0xFF,
            0xFF
        ])
        payloads.append([
            0xDE,
            0xAD,
            0xBE,
            0xEF,
            0,
            255,
            ] + (0..<255).map { $0 }
        )

        
        for payloadBytes in payloads {
            print(payloadBytes)
            let parsedPayload: AdvertisementPayload = AdvertisementPayload(payload: Data(bytes: payloadBytes, count: payloadBytes.count))
            XCTAssertFalse(parsedPayload.isValid())
        }
    }
}
