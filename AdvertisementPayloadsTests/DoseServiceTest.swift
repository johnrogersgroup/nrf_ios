//
//  DoseServiceTest.swift
//  AdvertisementPayloadsTests
//
//  Created by Jacob Trueb on 6/2/20.
//  Copyright © 2020 Jacob Trueb. All rights reserved.
//

import XCTest
import uv_guard

class DoseServiceTest: XCTestCase {

    override func setUpWithError() throws {
        ApiEndpoint.TEST = true
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func helper(_ before: [AdvertisementPayload], _ after: [AdvertisementPayload]) throws {
        print(before)
        var actualAfter: [AdvertisementPayload]!
            actualAfter = DoseService.consolidateDoses(uploadingPayloads: before)
        print("///////////////////////////////////////////")
        actualAfter.sort(by: { $0.getDeviceId() < $1.getDeviceId() })
        print(actualAfter!)
        assert(after == actualAfter)
    }
    
    func testConsolidateSingleDose() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        // Single Value
        let advs = [AdvertisementPayload(deviceId: 1, doses: [RecordedDose(recorded: Date(), value: 1, index: 0)])]
        try self.helper(advs, advs)
    }
    
    func testConsolidateModeDose() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        // Single Index with multiple values
        let advs = [
            AdvertisementPayload(deviceId: 1, doses: [RecordedDose(recorded: Date(), value: 1, index: 0)]),
            AdvertisementPayload(deviceId: 1, doses: [RecordedDose(recorded: Date(), value: 2, index: 0)]),
            AdvertisementPayload(deviceId: 1, doses: [RecordedDose(recorded: Date(), value: 2, index: 0)]),
            AdvertisementPayload(deviceId: 1, doses: [RecordedDose(recorded: Date(), value: 3, index: 0)]),
            AdvertisementPayload(deviceId: 1, doses: [RecordedDose(recorded: Date(), value: 3, index: 0)]),
            AdvertisementPayload(deviceId: 1, doses: [RecordedDose(recorded: Date(), value: 3, index: 0)])
        ]
        let cAdvs = [
            AdvertisementPayload(deviceId: 1, doses: [RecordedDose(recorded: Date(), value: 3, index: 0)])
        ]
        try self.helper(advs, cAdvs)
    }
    
    func testConsolidatePairOfDoses() throws {
        // Sequence of values
        let advs = [AdvertisementPayload(deviceId: 1, doses: [
            RecordedDose(recorded: Date(), value: 1, index: 5),
            RecordedDose(recorded: Date(), value: 1, index: 4)
        ])]
        try self.helper(advs, advs)
    }
    
    func testConsolidateSequencedDoses() throws {
        // Sequence of values
        let advs = [AdvertisementPayload(deviceId: 1, doses: [
            RecordedDose(recorded: Date(), value: 1, index: 5),
            RecordedDose(recorded: Date(), value: 1, index: 4),
            RecordedDose(recorded: Date(), value: 1, index: 3),
            RecordedDose(recorded: Date(), value: 1, index: 2),
            RecordedDose(recorded: Date(), value: 1, index: 1),
            RecordedDose(recorded: Date(), value: 1, index: 0)
        ])]
        try self.helper(advs, advs)
    }
    
    func testConsolidateOverlappingSequencedDoses() throws {
        // Sequence of values
        let advs = [
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 6),
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1)
            ])
        ]
        
        let cAdvs = [
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 6),
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0)
            ])
        ]
        try self.helper(advs, cAdvs)
    }
    
    func testConsolidateOverlappingSequencedDosesFromMultipleDevices() throws {
        // Sequence of values
        let advs = [
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 6),
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1)
            ]),
            AdvertisementPayload(deviceId: 2, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0)
            ]),
            AdvertisementPayload(deviceId: 2, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 7),
                RecordedDose(recorded: Date(), value: 1, index: 6),
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2)
            ])
        ]
        
        let cAdvs = [
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 6),
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0)
            ]),
            AdvertisementPayload(deviceId: 2, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 7),
                RecordedDose(recorded: Date(), value: 1, index: 6),
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0)
            ]),
        ]
        try self.helper(advs, cAdvs)
    }
    
    func testConsolidateNonOverlappingSequencedDoses() throws {
        // Sequence of values
        let advs = [
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 6),
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 15),
                RecordedDose(recorded: Date(), value: 1, index: 14),
                RecordedDose(recorded: Date(), value: 1, index: 13),
                RecordedDose(recorded: Date(), value: 1, index: 12),
                RecordedDose(recorded: Date(), value: 1, index: 11),
                RecordedDose(recorded: Date(), value: 1, index: 10)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 16),
                RecordedDose(recorded: Date(), value: 1, index: 15),
                RecordedDose(recorded: Date(), value: 1, index: 14),
                RecordedDose(recorded: Date(), value: 1, index: 13),
                RecordedDose(recorded: Date(), value: 1, index: 12),
                RecordedDose(recorded: Date(), value: 1, index: 11)
            ])
        ]
        
        let cAdvs = [
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 6),
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 16),
                RecordedDose(recorded: Date(), value: 1, index: 15),
                RecordedDose(recorded: Date(), value: 1, index: 14),
                RecordedDose(recorded: Date(), value: 1, index: 13),
                RecordedDose(recorded: Date(), value: 1, index: 12),
                RecordedDose(recorded: Date(), value: 1, index: 11),
                RecordedDose(recorded: Date(), value: 1, index: 10)
            ])
        ]
        try self.helper(advs, cAdvs)
    }
    
    func testConsolidateAdjacentOverlappingSequencedDoses() throws {
        // Sequence of values
        let advs = [
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 6),
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 15),
                RecordedDose(recorded: Date(), value: 1, index: 14),
                RecordedDose(recorded: Date(), value: 1, index: 13),
                RecordedDose(recorded: Date(), value: 1, index: 12),
                RecordedDose(recorded: Date(), value: 1, index: 11),
                RecordedDose(recorded: Date(), value: 1, index: 10)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 16),
                RecordedDose(recorded: Date(), value: 1, index: 15),
                RecordedDose(recorded: Date(), value: 1, index: 14),
                RecordedDose(recorded: Date(), value: 1, index: 13),
                RecordedDose(recorded: Date(), value: 1, index: 12),
                RecordedDose(recorded: Date(), value: 1, index: 11)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 8),
                RecordedDose(recorded: Date(), value: 1, index: 7)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 9)
            ])
        ]
        
        let cAdvs = [
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 16),
                RecordedDose(recorded: Date(), value: 1, index: 15),
                RecordedDose(recorded: Date(), value: 1, index: 14),
                RecordedDose(recorded: Date(), value: 1, index: 13),
                RecordedDose(recorded: Date(), value: 1, index: 12),
                RecordedDose(recorded: Date(), value: 1, index: 11),
                RecordedDose(recorded: Date(), value: 1, index: 10),
                RecordedDose(recorded: Date(), value: 1, index: 9),
                RecordedDose(recorded: Date(), value: 1, index: 8),
                RecordedDose(recorded: Date(), value: 1, index: 7),
                RecordedDose(recorded: Date(), value: 1, index: 6),
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0)
            ])
        ]
        try self.helper(advs, cAdvs)
    }
    
    func testConsolidateAdjacentWrappingOverlappingSequencedDoses() throws {
        // Sequence of values
        let advs = [
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 255),
                RecordedDose(recorded: Date(), value: 1, index: 254),
                RecordedDose(recorded: Date(), value: 1, index: 253),
                RecordedDose(recorded: Date(), value: 1, index: 252),
                RecordedDose(recorded: Date(), value: 1, index: 251),
                RecordedDose(recorded: Date(), value: 1, index: 250)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 0),
                RecordedDose(recorded: Date(), value: 1, index: 255),
                RecordedDose(recorded: Date(), value: 1, index: 254),
                RecordedDose(recorded: Date(), value: 1, index: 253),
                RecordedDose(recorded: Date(), value: 1, index: 252),
                RecordedDose(recorded: Date(), value: 1, index: 251)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2)
            ]),
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1)
            ]),
        ]
        
        let cAdvs = [
            AdvertisementPayload(deviceId: 1, doses: [
                RecordedDose(recorded: Date(), value: 1, index: 5),
                RecordedDose(recorded: Date(), value: 1, index: 4),
                RecordedDose(recorded: Date(), value: 1, index: 3),
                RecordedDose(recorded: Date(), value: 1, index: 2),
                RecordedDose(recorded: Date(), value: 1, index: 1),
                RecordedDose(recorded: Date(), value: 1, index: 0),
                RecordedDose(recorded: Date(), value: 1, index: 255),
                RecordedDose(recorded: Date(), value: 1, index: 254),
                RecordedDose(recorded: Date(), value: 1, index: 253),
                RecordedDose(recorded: Date(), value: 1, index: 252),
                RecordedDose(recorded: Date(), value: 1, index: 251)
            ])
        ]
        try self.helper(advs, cAdvs)
    }
}
